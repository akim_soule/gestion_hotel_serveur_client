package Controleur;


import Modele.Serveur;

/**
 * Classe app permettant de demarrer le serveur.
 * @author akimsoule
 *
 */
public class App {

	public static void main(String[] args) {
		
		Serveur serveur = new Serveur();
		serveur.start();

	}
}
