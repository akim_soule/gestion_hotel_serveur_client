package Modele;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.stream.Collectors;

import Modele.Donnees.DonneesServeur;
import Modele.classe_abstraite_dao.DAOFactory;
import Modele.objet.classe_liste.ListClientHotel;
import Modele.objet.classe_liste.ListConnectionEmployeAudit;
import Modele.objet.classe_liste.ListEmploye;
import Modele.objet.classe_liste.ListEmployeSuspendu;
import Modele.objet.classe_liste.ListHistoriqueSuspension;
import Modele.objet.classe_liste.ListReservation;
import Modele.objet.classe_liste.ListReservationAudit;
import Modele.package_enumeration.SIGNAL;
import Modele.package_logger.MyLoggingConnectionIPetPort;
import Modele.package_logger.MyLoggingMessageErreur;
import Modele.package_utilitaire.UtilitaireGestionReception;

/**
 * Classe representant un traitement client.
 * Elle a pour attributs le boolean permettant de savoir le client est connecte.
 * Le socket permettant la comminication.
 * Un utilitaireGestionReception permettant de gerer les reponses recues sur le serveur.
 * Un outputStream pour l'envoi des messages.
 * Un inputStream pour la reception des messages.
 * @author akimsoule
 *
 */
public class ThreadTraitementClient extends Thread {

	/**
	 * Le boolean permettant de savoir le client est connecte.
	 */
	private boolean modeConnecte;
	/**
	 * Le socket permettant le communication entre le client et le serveur.
	 */
	private Socket socket;
	/**
	 * Le login de l'employe.
	 */
	private String loginEmploye;
	/**
	 * L'utilitaireGestionReception permettant de gerer les messages.
	 */
	private UtilitaireGestionReception utilitaireGestionReception;
	/**
	 * L'outputstream pour l'envoi de messages.
	 */
	private ObjectOutputStream outputStream;
	/**
	 * L'inputStream pour la reception de messages.
	 */
	private ObjectInputStream inputStream;

	/**
	 * Constructeur de la classe.
	 * @param pSocketCourant
	 * le socket de communication.
	 */
	public ThreadTraitementClient(Socket pSocketCourant) {
		this.socket = pSocketCourant;
		
		try {
			outputStream = new ObjectOutputStream(socket.getOutputStream());
			inputStream = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		
		
		loginEmploye = new String();
		utilitaireGestionReception = new UtilitaireGestionReception();
		this.modeConnecte = false;
	}

	public void run() {

		synchronized (this) {

			System.out.println("Lancement du traitement de la connexion cliente");
			
			boolean closeConnexion = false;
			Object object = lire();
			// Object object = UtilitaireServeurReseau.recevoir();
			while (object != null) {

				try {
					String response = object.toString();

					System.out.println("Message recu sur le serveur :" + response);

					if (response.startsWith(SIGNAL.LOGIN.toString())) {
						this.modeConnecte = utilitaireGestionReception.gererLogin(this.modeConnecte, response,
								loginEmploye, this);
						if (this.modeConnecte == true) {
							loginEmploye = response.split("-")[1];
						}
					}
					if (this.modeConnecte == true) {
						if (response.split("-")[0].equals(SIGNAL.ROOT_SUPPRIME_EMPLOYESUSPENDU.toString())) {
							utilitaireGestionReception.gererSuppressionEmployeSuspendu(response,
									this);
						}
						if (response.split("-")[0].equals(SIGNAL.ROOT_SUPPRIME_EMPLOYE.toString())) {
							utilitaireGestionReception.gererSuppressionEmploye(response, this);

						}
						if (response.split("-")[0].equals(SIGNAL.ROOT_CREE_EMPLOYE.toString())) {
							utilitaireGestionReception.gererCreeEmploye(response, this);

						}
						if (response.split("-")[0].equals(SIGNAL.VUEGESTION_CREER_CLIENT.toString())) {
							utilitaireGestionReception.gererCreeClient(response, this);

						}
						if ((response.contains("DEMANDE"))) {
							this.envoyerObjet();
						}
						if (response.split("-")[0].equals(SIGNAL.VUEGESTION_SUPPRIME_CLIENT.toString())) {
							utilitaireGestionReception.gererSuppressionClient(response, this);

						}
						if (response.split("/")[0].equals(SIGNAL.VUEGESTION_SUPPRIME_RESERVATION.toString())) {
							utilitaireGestionReception.gererSuppressionReservation(response,
									this);
						}
						if (response.startsWith(SIGNAL.VUEGESTION_MODIFIER_CLIENT.toString())) {
							utilitaireGestionReception.gererModifierClient(response, this);

						}
						if (response.split("/")[0].equals(SIGNAL.VUEGESTION_CREER_RESERVATION.toString())) {
							utilitaireGestionReception.gererCreeReservation(response, this);

						}
						if (response.split("/")[0].equals(SIGNAL.VUEGESTION_MODIFIER_RESERVATION.toString())) {
							utilitaireGestionReception.gererModifierReservation(response, this);
						}
						if (response.equals(SIGNAL.VUEGESTION_DECONNEXION.toString())) {
							DAOFactory.getConnectionEmployeAuditDAO().insertInto_Connection_employe_audit("DISCONNECTED",
									loginEmploye);
							MyLoggingConnectionIPetPort.log(Level.INFO, "Deconnection recue du "+socket.getInetAddress().toString()+"-----"+socket.getPort());
				
							closeConnexion = true;
							this.modeConnecte = false;
						}
					}
					if (closeConnexion) {
						this.closeConnection();
						break;

					} else {
						System.out.println("En attente de la reponse d'un message");
						object = lire();
					}
				} catch (SocketException e) {
					System.out.println("La connexion a ete interrompu");
					break;
				} catch (IOException e) {
					e.printStackTrace();
					MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
				}

			}
		}

	}
	
	/**
	 * Methode permettant d'envoyer des objets.
	 * @param object
	 * les objets a envoyer.
	 */
	public synchronized void envoyer(Object object) {
		if (object.getClass() == String.class)
			System.out.println("Ce message a ete envoye" + object);
		
		try {
			if (object != null) {
				outputStream.writeObject(object);
				outputStream.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
	}

	/**
	 * Methode permettant de lire les objets.
	 * @return
	 * l'objet lu.
	 */
	public synchronized Object lire() {
		Object object = null;
		try {
			object = inputStream.readObject();
		} catch (ClassNotFoundException e) {
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		} catch (IOException e) {
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return object;
	}

	/**
	 * Methode permettant de fermer la connection.
	 * @throws IOException
	 * L'exception lancee par la fermetture de la connection.
	 */
	public synchronized void closeConnection() throws IOException {
		outputStream = null;
		inputStream = null;
		socket.close();
	}

	/**
	 * Methode permettant d'envoyer des objets au client.
	 */
	public synchronized void envoyerObjet() {
		DonneesServeur.lire();
		envoyer(new ListReservation(DonneesServeur.obserListReservations.stream().collect(Collectors.toList())));
		envoyer(new ListReservationAudit(
				DonneesServeur.obserListReservationAudits.stream().collect(Collectors.toList())));
		envoyer(new ListClientHotel(DonneesServeur.obserListClientHotels.stream().collect(Collectors.toList())));
		envoyer(new ListEmploye(DonneesServeur.obserListEmployes.stream().collect(Collectors.toList())));
		envoyer(new ListEmployeSuspendu(
				DonneesServeur.obserListEmployeSuspendus.stream().collect(Collectors.toList())));
		envoyer(new ListHistoriqueSuspension(
				DonneesServeur.obserListHistoriqueSuspensions.stream().collect(Collectors.toList())));

		envoyer(new ListConnectionEmployeAudit(
				DonneesServeur.obserListConnectionEmployeAudits.stream().collect(Collectors.toList())));

	}
}
