package Modele.package_logger;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.sun.security.ntlm.Client;

/**
 * Classe permettant de representer le logger pour l'enregistrement des adresses ip et des ports.
 * @author akimsoule
 *
 */
public class MyLoggingConnectionIPetPort {

	/**
	 * Le logger.
	 */
	private static Logger logger;
	/**
	 * Le le file.
	 */
	private Handler fileHandler;
	/**
	 * Le formateur.
	 */
	private Formatter plainText;

	/**
	 * Principal constructeur de la classe permettant de definir le logger, 
	 * le fichier ou les logs seront enregistres et le formateur. 
	 */
	public MyLoggingConnectionIPetPort() {
		logger = Logger.getLogger(Client.class.getName());
		try {
			fileHandler = new FileHandler("LOG_CLIENT_CONNEXION_DECONNEXION"+".txt", true);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
		plainText = new SimpleFormatter();
		fileHandler.setFormatter(plainText);
		logger.addHandler(fileHandler);

	}

	/**
	 * Methode permettant de recuperer le logger courant.
	 * @return 
	 * le logger courant.
	 */
	private static Logger getLogger() {
		if (logger == null) {
			new MyLoggingConnectionIPetPort();
		}
		return logger;
	}

	/**
	 * Methode permettant de loger dans le fichier.
	 * @param level
	 * Le niveau du log.
	 * @param msg
	 * Le message a enregistrer.
	 */
	public static void log(Level level, String msg) {
		Logger logg = getLogger();
		if (logg != null) {
			getLogger().log(level, msg);
			if (level.equals(Level.WARNING)) {
				System.err.println(msg);
			}
		}
		
	}

}
