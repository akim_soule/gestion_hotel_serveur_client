package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.classe_abstraite_dao.DAOFactory;
import Modele.objet.classe_java.EmployeSuspendu;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe representant permettant d'interagir avec la table employe de la base de donnees.
 * @author akimsoule
 *
 */
public class EmployeSuspenduDAO extends DAO<EmployeSuspendu> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public EmployeSuspenduDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode permettant d'inserer un employe suspendu.
	 * @param login
	 * le login a inserer.
	 * @return
	 * vrai si le login a ete suspendu et faux dans le cas contraire
	 */
	public synchronized boolean insertInto_Employe_suspendu(String login) {
		String sql2 = "insert into employe_suspendu (login_employe_suspendu) values (?)";
		List<Object> list2 = new ArrayList<Object>();
		list2.add(login);
		return executerRequette(sql2, list2);
	}

	/**
	 * Methode permettant de verifier si un employe a ete suspendu.
	 * @param login
	 * le login a verifier.
	 * @return
	 * vrai si le login a ete suspendu et faux dans le cas contraire.
	 */
	public synchronized boolean verifierSiUnEmployeEstSupendu(String login) {
		if (selectIdEmployeFromEmployeSuspenduWhere(login) > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode permettant de recuperer l'id de l'employe suspendu.
	 * @param login
	 * le login dont on veut recuperer l'id.
	 * @return
	 * l'id du login.
	 */
	private synchronized int selectIdEmployeFromEmployeSuspenduWhere(String login) {
		String sql6 = "Select id_employe_suspendu from employe_suspendu " + "where login_employe_suspendu = '" + login
				+ "' ";
		ResultSet resultSet4 = LireBD(sql6);
		int idEmploye = 0;
		try {
			if (resultSet4.next()) {
				idEmploye = resultSet4.getInt("id_employe_suspendu");
			} else {
				idEmploye = -1;
			}
		} catch (SQLException e) {
			idEmploye = -1;
		}
		return idEmploye;
	}

	/**
	 * Methode permettant de consulter la liste des employes suspendus.
	 * @return
	 * le liste des employes suspendus.
	 */
	public synchronized List<EmployeSuspendu> consulterBDEmployesSuspendus() {
		List<EmployeSuspendu> list = new ArrayList<EmployeSuspendu>();
		String sql = "Select * from employe_suspendu";
		ResultSet resultSet = LireBD(sql);
		try {
			while (resultSet.next()) {
				int id = resultSet.getInt("id_employe_suspendu");
				String login = resultSet.getString("login_employe_suspendu");
				list.add(new EmployeSuspendu(id, login));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}

		return list;
	}

	/**
	 * Methode permettant de supprimer un login.
	 * @param loginASupprimer
	 * le login qu'on veut supprimer.
	 * @return
	 * vrai si le login a ete supprimer et faux dans le cas contraire.
	 */
	public synchronized boolean supprimerEmploye(String loginASupprimer) {
		String sql = "delete from employe_suspendu where login_employe_suspendu = ?";
		DAOFactory.getConnectionTentative().remetreAZerTentative(loginASupprimer);
		List<Object> list = new ArrayList<Object>();
		list.add(loginASupprimer);
		return executerRequette(sql, list);
	}

}
