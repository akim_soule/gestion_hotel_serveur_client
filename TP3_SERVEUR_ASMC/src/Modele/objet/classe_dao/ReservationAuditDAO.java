package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.ReservationAudit;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe representant permettant d'interagir avec la table reservation_audit de la base de donnees.
 * @author akimsoule
 *
 */
public class ReservationAuditDAO extends DAO<ReservationAudit> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public ReservationAuditDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode permettant de recuperer la liste des objets reservationAudit.
	 * @return
	 * la liste des objets reservationAudit.
	 */
	public  synchronized List<ReservationAudit> lireTable() {
		String sql = "select * from reservation_audit";
		List<ReservationAudit> list = new ArrayList<ReservationAudit>();
		ResultSet resultSet = LireBD(sql);
		try {
			while (resultSet.next()) {
				list.add(new ReservationAudit(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getInt(5), resultSet.getInt(6), resultSet.getString(7),
						resultSet.getInt(8), resultSet.getString(9).substring(0, 19)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}

		return list;
	}
}
