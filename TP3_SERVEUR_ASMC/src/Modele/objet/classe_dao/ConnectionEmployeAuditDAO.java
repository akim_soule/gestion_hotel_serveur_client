package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.ConnectionEmployeAudit;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe representant permettant d'interagir avec la table connection_employe_audit de la base de donnees.
 * @author akimsoule
 *
 */
public class ConnectionEmployeAuditDAO extends DAO<ConnectionEmployeAudit> {
	
	
	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public ConnectionEmployeAuditDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode permettant la table connection_employe_audit
	 * @return
	 * la liste des objets connection_employe_audit.
	 */
	public synchronized List<ConnectionEmployeAudit> lireTable() {
		String sql = "select * from connection_employe_audit";
		List<ConnectionEmployeAudit> list = new ArrayList<ConnectionEmployeAudit>();
		ResultSet resultSet = LireBD(sql);
		try {
			while (resultSet.next()) {
				list.add(new ConnectionEmployeAudit(resultSet.getString(1), resultSet.getString(2), 
						resultSet.getString(3).substring(0, 19)));
			}
		} catch (SQLException e) {

			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return list;
	}
	
	/**
	 * Methode permettant d'inserer un enregistrement dans la table connection_employe_audit.
	 * @param string
	 * la chaine (CONNECTED ou DISCONNECTED).
	 * @param loginEmploye
	 * le login de l'employe.
	 * @return
	 * vrai si l'insertion a ete faite et faux dans le cas contraire.
	 */
	public synchronized boolean insertInto_Connection_employe_audit(String string, String loginEmploye) {
		String sql2 = "insert into connection_employe_audit (login_employe, connection_audit) values (?, ?)";
		List<Object> list2 = new ArrayList<Object>();
		list2.add(loginEmploye);
		list2.add(string);
		return executerRequette(sql2, list2);

	}

}
