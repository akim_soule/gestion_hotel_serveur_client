package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.ConnectionTentative;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe representant permettant d'interagir avec la table connection_tentative de la base de donnees.
 * @author akimsoule
 *
 */
public class ConnectionTentativeDAO extends DAO<ConnectionTentative> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public ConnectionTentativeDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode permettent d'incrementer le nombre de tentatives
	 * @param login
	 * le login qui echoue en se connectant
	 * @return
	 * vrai si l'incrementation est faite et faux dans le cas contraire.
	 */
	public synchronized boolean incrementerNombre(String login) {
		boolean ok = false;

		List<Object> list = new ArrayList<Object>();
		if (verifierSiUnEmployeADejaEssaye(login)) {

			int nombreCourant = getTentative(login);
			if (nombreCourant == 3) {
				ok = false;
				list.add(nombreCourant);
			} else {
				list.add(nombreCourant + 1);
				ok = true;
			}
			list.add(login);

			String sql = "update connection_tentative set nombre_connection_tentative = ? where login_connection_tentative = ?";
			executerRequette(sql, list);

		} else {
			list.add(login);
			list.add(1);
			ok = true;
			String sql = "insert into connection_tentative (login_connection_tentative, nombre_connection_tentative) values (?, ?)";
			executerRequette(sql, list);
		}

		return ok;

	}

	/**
	 * Methode permettant de recuperer le nombre de tentatives
	 * @param login
	 * le login qui a deja eu tel nombre d'echecs lors de la connexion.
	 * @return
	 * le nombre de tentatives.
	 */
	public synchronized int getTentative(String login) {
		String sql = "select nombre_connection_tentative from connection_tentative where login_connection_tentative = '"
				+ login + "' ";
		ResultSet resultSet = LireBD(sql);
		int i = 0;
		try {
			if (resultSet.next()) {
				i = resultSet.getInt("nombre_connection_tentative");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}

		return i;

	}

	/**
	 * Methode permettant de verifier si un employe a deja essaye
	 * donc present dans la table de connection_tentative.
	 * @param login
	 * le login cense etre present dans la table connection_tentative.
	 * @return
	 * vrai s'il est present et faux dans le cas contraire.
	 */
	public synchronized boolean verifierSiUnEmployeADejaEssaye(String login) {
		if (selectIdEmployeFromEmployeWhere(login) > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode permettant de recuperer l'id du login dans la table connection_tentative.
	 * @param loginEmploye
	 * le login cense etre present dans la table connection_tentative.
	 * @return
	 * vrai s'il est present et faux dans le cas contraire.
	 */
	public synchronized int selectIdEmployeFromEmployeWhere(String loginEmploye) {
		String sql = "Select id_connection_tentative from connection_tentative "
				+ "where login_connection_tentative = '" + loginEmploye + "' ";
		ResultSet resultSet4 = LireBD(sql);
		int idEmploye = 0;
		try {
			if (resultSet4.next()) {
				idEmploye = resultSet4.getInt("id_connection_tentative");
			} else {
				idEmploye = -1;
			}
		} catch (SQLException e) {
			idEmploye = -1;
		}
		return idEmploye;

	}

	/**
	 * Methode permettant de remettre a zero le nombre de tentatives du login.
	 * @param loginASupprimer
	 * le login dont le nombre de tentatives doit etre remis a zero.
	 * @return
	 * vrai si la requette est effectuee et faux dans le cas contraire.
	 */
	public synchronized boolean remetreAZerTentative(String loginASupprimer) {
		String sql = "update connection_tentative set nombre_connection_tentative = 0 where login_connection_tentative = ?";

		List<Object> list = new ArrayList<Object>();
		list.add(loginASupprimer);
		return executerRequette(sql, list);

	}

	/**
	 * Methode permettant de supprimer un user de la table connection_tentative.
	 * @param login
	 * le login a supprimer de la table connection_tentative.
	 * @return
	 * vrai si le user a ete supprime et faux dans le cas contraire.
	 */
	public synchronized boolean deleteUser(String login) {
		String sql = "delete from connection_tentative where login_connection_tentative = ?";
		List<Object> list = new ArrayList<Object>();
		list.add(login);
		return executerRequette(sql, list);
		
	}

}
