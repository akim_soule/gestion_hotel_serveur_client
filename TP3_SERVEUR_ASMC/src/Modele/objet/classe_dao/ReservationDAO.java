package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.Reservation;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe representant permettant d'interagir avec la table reservation de la base de donnees.
 * @author akimsoule
 *
 */
public class ReservationDAO extends DAO<Reservation> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public ReservationDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode de consulter la liste des objets reservation.
	 * @return
	 * la liste des objets reservation.
	 */
	public synchronized List<Reservation> consulterBDReservation() {
		List<Reservation> list = new ArrayList<Reservation>();

		String sql = "Select * from reservation";
		ResultSet resultSet = LireBD(sql);

		try {
			while (resultSet.next()) {
				int id = resultSet.getInt("id_reservation");
				String date_de_debut_sejour = resultSet.getString("date_de_debut_sejour").substring(0, 19);
				String date_de_fin_sejour = resultSet.getString("date_de_fin_sejour").substring(0, 19);
				int nombre_enfant = resultSet.getInt("nombre_enfant");
				int nombre_adulte = resultSet.getInt("nombre_adulte");
				int id_employe = resultSet.getInt("id_employe");
				int idClient = resultSet.getInt("id_client");
				int numeroChambre = resultSet.getInt("chambre");
				String sql3 = "select * from client where id_client = '" + idClient + "'";
				ResultSet resultSet3 = LireBD(sql3);
				String nom_client = "";
				String prenom_client = "";
				try {
					while (resultSet3.next()) {
						nom_client = resultSet3.getString("nom_client");
						prenom_client = resultSet3.getString("prenom_client");
						break;
					}

				} catch (SQLException e) {
					e.printStackTrace();
					MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
				}

				list.add(new Reservation(id, nom_client, prenom_client, date_de_debut_sejour, date_de_fin_sejour,
						nombre_enfant, nombre_adulte, id_employe, idClient, numeroChambre));

			}

		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return list;
	}

	/**
	 * Methode permettant de supprimer une reservation.
	 * @param id
	 * l'id dont on veut supprimer la reservation.
	 * @param userNom
	 * le nom de la personne qui supprimer la reservation.
	 * @param dateDebut
	 * la date de debut reservation.
	 * @param dateFin
	 * la date de fin reservation.
	 * @param nombre_enfant
	 * le nombre d'enfants
	 * @param nombre_adulte
	 * le nombre d'adultes.
	 * @param i
	 * le numero de la chambre.
	 * 
	 */
	public synchronized void supprimerReservation(int id, String userNom, String dateDebut, String dateFin,
			int nombre_enfant, int nombre_adulte, int i) {

		String sql3 = "select concat(nom_client, ', ', prenom_client) as nom_prenom_client from client where id_client = (select id_client from reservation where id_reservation = "
				+ id + ")";
		ResultSet resultSet3 = LireBD(sql3);
		String nom_employe_client = "";
		try {
			while (resultSet3.next()) {
				nom_employe_client = resultSet3.getString("nom_prenom_client");
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		insertIntoReservationAudit(userNom, "SUPPRESSION", dateDebut, dateFin, nombre_enfant, nombre_adulte,
				nom_employe_client, i);

		String sql = "delete from reservation where id_reservation = ?";
		List<Object> list = new ArrayList<Object>();
		list.add(id);
		executerRequette(sql, list);
	}

	/**
	 * Methode permettant d'inserer une reservation.
	 * @param userNom
	 * le login de celuit supprime la reservation.
	 * @param string
	 * le string peut etre CREATION, MODIFICATION, SUPPRESSION.
	 * @param dateDebut
	 * la date de debut reservation.
	 * @param dateFin
	 * la date de fin reservation.
	 * @param nombre_enfant
	 * le nombre d'enfants.
	 * @param nombre_adulte
	 * le nombre d'adultes.
	 * @param nom_employe_client
	 * le nom prenom concatene du client.
	 * @param i
	 * le numero de la chambre.
	 */
	private synchronized void insertIntoReservationAudit(String userNom, String string, String dateDebut,
			String dateFin, int nombre_enfant, int nombre_adulte, String nom_employe_client, int i) {
		String sql2 = "INSERT INTO "
				+ "reservation_audit (acteur , type_update , debut_audit , fin_audit, nombre_enfant_audit, nombre_adulte_audit, nom_prenom_client_audit, chambre_audit) "
				+ "VALUES (?,?,?,?,?,?,?,?)";

		List<Object> list2 = new ArrayList<Object>();
		list2.add(userNom);
		list2.add(string);
		list2.add(dateDebut);
		list2.add(dateFin);
		list2.add(nombre_enfant);
		list2.add(nombre_adulte);
		list2.add(nom_employe_client);
		list2.add(i);

		executerRequette(sql2, list2);

	}

	/**
	 * Methode permettant de faire une reservation.
	 * @param id_reservation
	 * l'id de reservation.
	 * @param dateDebut
	 * la date de debut.
	 * @param dateFin
	 * la date de fin.
	 * @param numeroChambre
	 * le numero de la chambre.
	 * @param nombreAdulte
	 * le nombre d'adultes.
	 * @param nombreEnfant
	 * le nombre d'enfants.
	 * @param userNom
	 * le login qui modifie la reservation.
	 * @param nomPrenomClient
	 * le nom et prenom concatene du client.
	 */
	public synchronized void modifierReservation(int id_reservation, String dateDebut, String dateFin,
			int numeroChambre, int nombreAdulte, int nombreEnfant, String userNom, String nomPrenomClient) {

		String sql = "update reservation set " + "date_de_debut_sejour = ? , " + "date_de_fin_sejour = ? , "
				+ "nombre_enfant = ? , " + "nombre_adulte = ? , " + "chambre = ? where id_reservation = ?";

		List<Object> list = new ArrayList<Object>();

		list.add(dateDebut);
		list.add(dateFin);
		list.add(nombreEnfant);
		list.add(nombreAdulte);
		list.add(numeroChambre);
		list.add(id_reservation);

		executerRequette(sql, list);

		String sql2 = "INSERT INTO "
				+ "reservation_audit (acteur , type_update , debut_audit , fin_audit, nombre_enfant_audit, nombre_adulte_audit, nom_prenom_client_audit, chambre_audit) "
				+ "VALUES (?,?,?,?,?,?,?,?)";

		List<Object> list2 = new ArrayList<Object>();
		list2.add(userNom);
		list2.add("MODICATION");
		list2.add(dateDebut);
		list2.add(dateFin);
		list2.add(nombreEnfant);
		list2.add(nombreAdulte);
		list2.add(nomPrenomClient);
		list2.add(numeroChambre);

		executerRequette(sql2, list2);

	}

	/**
	 * Methode permettant de faire une reservation.
	 * @param numeroChambre
	 * le numero de la chambre.
	 * @param idClient
	 * l'id du client.
	 * @param dateFin
	 * la date de fin.
	 * @param dateDebut
	 * la date de debut.
	 * @param loginEmploye
	 * le login de l'employe.
	 * @param nomClient
	 * le nom du client.
	 * @param prenomClient
	 * le prenom du client.
	 * @param nombreAdulte
	 * le nombre d'adultes.
	 * @param nombreEnfant
	 * le nombre d'enfants.
	 */
	public synchronized void faireUnereservation(int numeroChambre, int idClient, String dateFin,
			String dateDebut, String loginEmploye, String nomClient, String prenomClient, int nombreAdulte,
			int nombreEnfant) {
		EmployeDAO employe = new EmployeDAO(this.connect);
		int idEmploye = employe.selectIdEmployeFromEmployeWhere(loginEmploye);
		
		String sql4 = "insert into reservation " + "(" + "date_de_debut_sejour, " + "date_de_fin_sejour, "
				+ "nombre_enfant, " + "nombre_adulte, " + "id_employe, " + "id_client, " + "chambre) " + "values "
				+ "(?, ?, ?, ?, ?, ?, ?)";

		List<Object> list2 = new ArrayList<Object>();
		list2.add(dateDebut);
		list2.add(dateFin);
		list2.add(nombreEnfant);
		list2.add(nombreAdulte);
		list2.add(idEmploye);
		list2.add(idClient);
		list2.add(numeroChambre);
		executerRequette(sql4, list2);

	}

}
