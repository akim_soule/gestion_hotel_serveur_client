package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.Chambre;

/**
 * Classe representant permettant d'interagir avec la table chambre de la base de donnees.
 * @author akimsoule
 *
 */
public class ChambreDAO extends DAO<Chambre> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public ChambreDAO(Connection conn) {
		super(conn);
	}
	
	/**
	 * Methode permettant de recuperer la liste des chambres prises.
	 * @return
	 * la liste des chambres.
	 */
	public synchronized List<Chambre> demandeChambrePrise() {
		List<Chambre> listChambrePrise = new ArrayList<Chambre>();
		String sql = "select chambre, date_de_debut_sejour, date_de_fin_sejour from reservation";
		ResultSet resultSet = LireBD(sql);

		try {
			while (resultSet.next()) {
				int numero = resultSet.getInt("chambre");
				String dateDebut = resultSet.getString("date_de_debut_sejour");
				String dateFin = resultSet.getString("date_de_fin_sejour");
				dateDebut = dateDebut.replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "").trim();
				dateFin = dateFin.replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "").trim();

				Chambre chambre = new Chambre(numero);
				chambre.setDateTimeDebutEtFin(dateDebut, dateFin);

				listChambrePrise.add(chambre);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listChambrePrise;
	}


}
