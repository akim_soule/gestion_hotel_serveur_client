package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.ClientHotel;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe representant permettant d'interagir avec la table client de la base de donnees.
 * @author akimsoule
 *
 */
public class ClientDAO extends DAO<ClientHotel> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public ClientDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode permettant d'inserer un client dans la BD
	 * @param pNomClient
	 * le nom du client.
	 * @param pPrenomClient
	 * la prenom du client.
	 */
	public synchronized void insertIntoClient(String pNomClient, String pPrenomClient) {
		String query = " insert into client (nom_client, prenom_client)" + " values (?, ?)";
		List<Object> list = new ArrayList<Object>();
		list.add(pNomClient);
		list.add(pPrenomClient);
		executerRequette(query, list);
	}

	/**
	 * Methode permettant de verifier si un client existe.
	 * @param nom
	 * le nom du client.
	 * @param prenom
	 * le prenom du client.
	 * @return
	 * vrai si le client existe.
	 */
	public synchronized boolean verifierSiLeCLientExiste(String nom, String prenom) {
		boolean trouve = false;

		if (selectIdClientFromClientWhere(nom, prenom) >= 0) {
			trouve = true;
		} else {
			trouve = false;
		}
		return trouve;

	}

	/**
	 * Methode permettant de recuperer l'id du client
	 * @param nom
	 * le nom du client.
	 * @param prenom
	 * le prenom du client.
	 * @return
	 * l'id du client.
	 */
	private synchronized int selectIdClientFromClientWhere(String nom, String prenom) {
		String sql = "Select id_client from client " + "where nom_client = '" + nom + "' " + "and  prenom_client = '"
				+ prenom + "'";
		ResultSet resultSet = LireBD(sql);
		int idClient = 0;
		try {
			if (resultSet.next()) {
				idClient = resultSet.getInt("id_client");
			} else {
				idClient = -1;
			}
		} catch (SQLException e) {
			idClient = -1;
		}

		return idClient;

	}

	/**
	 * Methode permettant de recuperer la liste des clients.
	 * @return
	 * la liste des clients.
	 */
	public synchronized List<ClientHotel> consulterBDClient() {

		ResultSet resultSet = null;
		List<ClientHotel> list = new ArrayList<ClientHotel>();

		String sql = "Select * from client";
		resultSet = LireBD(sql);

		try {
			while (resultSet.next()) {
				list.add(new ClientHotel(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
			}
		} catch (SQLException e) {

			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return list;
	}

	/**
	 * Methode de supprimer un client.
	 * @param nom
	 * le nom du client.
	 * @param prenom
	 * le prenom du client.
	 * @return
	 * vrai si la suppression a ete effectuee et faux dans le cas contraire.
	 */
	public synchronized boolean supprimerClient(String nom, String prenom) {
		boolean resultat = false;
		String sql2 = "select id_reservation from reservation "
				+ "inner join client on reservation.id_client = client.id_client where " + "client.nom_client = '" + nom
				+ "' and client.prenom_client = '" + prenom + "'";

		ResultSet resultSet = LireBD(sql2);
		boolean trouve = false;
		try {
			while (resultSet.next()) {
				trouve = true;
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}

		if (trouve) {
			resultat = false;
		} else {
			String sql = "delete from client where nom_client = ? and prenom_client = ?";
			List<Object> list = new ArrayList<Object>();
			list.add(nom);
			list.add(prenom);
			executerRequette(sql, list);
			resultat = true;
		}
		return resultat;

	}


	/**
	 * Methode permettant de mettre a jour un client
	 * @param nouveauNom
	 * le nouveau nom du client.
	 * @param nouveauPrenom
	 * le nouveau prenom.
	 * @param ancienNom
	 * l'ancien nom.
	 * @param ancienPrenom
	 * l'ancien prenom.
	 */
	public synchronized void mettreAJourClient(String nouveauNom, String nouveauPrenom, String ancienNom,
			String ancienPrenom) {

		int idClient = selectIdClientFromClientWhere(ancienNom, ancienPrenom);

		String sql2 = "update client set nom_client = ? , prenom_client = ? " + "where id_client = ?";
		List<Object> list = new ArrayList<Object>();

		list.add(nouveauNom);
		list.add(nouveauPrenom);
		list.add(idClient);
		executerRequette(sql2, list);
	}

}
