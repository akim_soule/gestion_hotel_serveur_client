package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.Employe;
import Modele.package_logger.MyLoggingMessageErreur;


/**
 * Classe representant permettant d'interagir avec la table employe de la base de donnees.
 * @author akimsoule
 *
 */
public class EmployeDAO extends DAO<Employe> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public EmployeDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode permettant de verifier si un employe existe.
	 * @param login
	 * la login dont on veut verifier s'il existe.
	 * @return
	 * vrai si le user existe et faux dans le cas contraire.
	 */
	public synchronized boolean verifierSiUnEmployeExiste(String login) {
		if (selectIdEmployeFromEmployeWhere(login) > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode permettant d'inserer un employe et son mot de passe dans la bd.
	 * @param loginEmploye
	 * le login a inserer
	 * @param pPasswordEmploye
	 * le mot de passe a inserer
	 * @return
	 * vrai si l'insertion s'est bien deroulee et faux dans la cas contraire.
	 */
	public synchronized boolean insertIntoEmploye(String loginEmploye, String pPasswordEmploye) {
		boolean ok = false;
		boolean bool = verifierSiUnEmployeExiste(loginEmploye);
		if (!bool) {
			String query = " insert into employe (login_employe, password_employe)" + " values (?, ?)";
			List<Object> list = new ArrayList<Object>();
			list.add(loginEmploye);
			list.add(pPasswordEmploye);
			executerRequette(query, list);
			ok = true;
		}
		return ok;
	}
	

	/**
	 * Methode permettant de selectionner l'id de l'employe
	 * @param loginEmploye
	 * le login dont on veut connaitre l'id
	 * @return
	 * l'id de l'employe
	 */
	public synchronized int selectIdEmployeFromEmployeWhere(String loginEmploye) {
		String sql6 = "Select id_employe from employe " + "where login_employe = '" + loginEmploye + "' ";
		ResultSet resultSet4 = LireBD(sql6);
		int idEmploye = 0;
		try {
			if (resultSet4.next()) {
				idEmploye = resultSet4.getInt("id_employe");
			} else {
				idEmploye = -1;
			}
		} catch (SQLException e) {
			idEmploye = -1;
		}
		return idEmploye;

	}

	/**
	 * Methode permettant de recuperer la liste des employe de la table employe
	 * @return
	 * la liste des employes
	 */
	public synchronized List<Employe> consulterBDEmployes() {
		List<Employe> list = new ArrayList<Employe>();
		String sql = "Select * from employe";
		ResultSet resultSet = LireBD(sql);
		try {
			while (resultSet.next()) {
				int id = resultSet.getInt("id_employe");
				String login = resultSet.getString("login_employe");
				list.add(new Employe(id, login));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}

		return list;

	}

	/**
	 * Methode permettant de savoir si le mot de passe est le bon
	 * @param login
	 * le login dont on veut verifier le mot de passe
	 * @param passwordHashe
	 * le password qu'on veut verifier
	 * @return
	 * vrai si le mot de passe est le bon et faux dans le cas contraire.
	 */
	public synchronized boolean estCeQueLeMotDePasseEstLeBon(String login, String passwordHashe) {
		boolean ok = false;
		String sql = "select password_employe from employe where login_employe = '" + login + "'";

		ResultSet resultSet = LireBD(sql);
		String passwordDeLaBaseDeDonnee = "";
		try {
			while (resultSet.next()) {
				passwordDeLaBaseDeDonnee = resultSet.getString("password_employe");
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		if (passwordDeLaBaseDeDonnee.equals(passwordHashe)) {
			ok = true;
		}
		return ok;
	}
	
	/**
	 * Methode permettant de supprimer un employe
	 * @param login
	 * le login qu'on veut supprimer
	 * @return
	 * vrai si le login a ete supprime et faux dans le cas contraire.
	 */
	public synchronized boolean supprimerEmploye(String login) {
		String sql = "delete from employe where login_employe = ?";
		
		List<Object> list = new ArrayList<Object>();
		list.add(login);
		return executerRequette(sql, list);
		
	}


}
