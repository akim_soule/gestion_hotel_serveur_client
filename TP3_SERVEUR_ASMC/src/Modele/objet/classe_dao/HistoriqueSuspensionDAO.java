package Modele.objet.classe_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAO;
import Modele.objet.classe_java.HistoriqueSuspension;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe permettant d'interagir avec la table employe_suspendu_audit de la base de donnees.
 * @author akimsoule
 *
 */
public class HistoriqueSuspensionDAO extends DAO<HistoriqueSuspension> {

	/**
	 * Constructeur permettant de se connecter a la bd.
	 * @param conn
	 * la connection a la bd.
	 */
	public HistoriqueSuspensionDAO(Connection conn) {
		super(conn);
	}

	/**
	 * Methode permettant de recuperer la liste des objets historiqueSuspension.
	 * @return
	 * la liste des objets historique suspension.
	 */
	public synchronized List<HistoriqueSuspension> consulterBDHistoriqueSuspension() {
		List<HistoriqueSuspension> list = new ArrayList<HistoriqueSuspension>();
		String sql = "Select * from employe_suspendu_audit";
		ResultSet resultSet = LireBD(sql);
		try {
			while (resultSet.next()) {
				int id = resultSet.getInt("id_employe_suspendu_audit");
				String login = resultSet.getString("login_employe_suspendu_audit");
				String audit = resultSet.getString("varchar_");
				String date = resultSet.getString("date_de_declenchement").substring(0, 19);
				
				list.add(new HistoriqueSuspension(id, login, audit, date));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}

		return list;
	}
	

}
