package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.Employe;

/**
 * Classe permettant de representer la liste de employes.
 * Ella pour principal attribut la liste des employes.
 * @author akimsoule
 *
 */
public class ListEmploye implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La liste des employes.
	 */
	private List<Employe> listEmploye = null;

	/**
	 * Principal constructeur de la classe.
	 * @param listEmploye
	 * la liste des employes.
	 */
	public ListEmploye(List<Employe> listEmploye) {
		this.listEmploye = listEmploye;
	}

	public List<Employe> getListEmploye() {
		return listEmploye;
	}

	public void setListEmploye(List<Employe> listEmploye) {
		this.listEmploye = listEmploye;
	}
	

}
