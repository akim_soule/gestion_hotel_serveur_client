package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.Reservation;

/**
 * Classe representant la liste des reservations.
 * Elle a pour principal attribut la liste des reservations.
 * @author akimsoule
 *
 */
public class ListReservation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La liste des reservations.
	 */
	private List<Reservation> listReservations = null;

	/**
	 * Principal constructeur de la classe.
	 * @param listReservations
	 * La liste des reservations.
	 */
	public ListReservation(List<Reservation> listReservations) {
		super();
		this.listReservations = listReservations;
	}

	public List<Reservation> getListReservations() {
		return listReservations;
	}

	public void setListReservations(List<Reservation> listReservations) {
		this.listReservations = listReservations;
	}

	@Override
	public String toString() {
		return "ListReservation [listReservations=" + listReservations + "]";
	}
	
	
	
	
}
