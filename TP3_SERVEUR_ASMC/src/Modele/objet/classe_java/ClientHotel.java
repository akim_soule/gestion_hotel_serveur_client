package Modele.objet.classe_java;

import java.io.Serializable;

/**
 * Classe permettant de representer un client de l'hotel.
 * Elle a pour principals attributs l'id, le nom et le prenom.
 * @author akimsoule
 *
 */
public class ClientHotel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * L'id du client.
	 */
	private int id_client;
	/**
	 * Le nom du client.
	 */
	private String nom_client;
	/**
	 * Le prenom du client.
	 */
	private String prenom_client;
	
	/**
	 * Le constructeur du client de l'hotel.
	 * @param id_client
	 * l'id du client.
	 * @param nom_client
	 * le nom du client.
	 * @param prenom_client
	 * le prenom du client.
	 */
	public ClientHotel(int id_client, String nom_client, String prenom_client) {
		super();
		this.id_client = id_client;
		this.nom_client = nom_client;
		this.prenom_client = prenom_client;
	}


	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	public String getNom_client() {
		return nom_client;
	}

	public void setNom_client(String nom_client) {
		this.nom_client = nom_client;
	}

	public String getPrenom_client() {
		return prenom_client;
	}

	public void setPrenom_client(String prenom_client) {
		this.prenom_client = prenom_client;
	}
	@Override
	public String toString() {
		return nom_client+", "+prenom_client;
	}
	
	public String toStringId() {
		return id_client+", "+nom_client+", "+prenom_client;
	}
	

}
