package Modele.objet.classe_java;

import java.io.Serializable;

/**
 * Classe representant la reservation audit.
 * Elle a pour principals attributs l'acteur qui a fait la reservation,
 * le type d'update (CREATION ou SUPPRESSION),
 * la date de debut sejour,
 * la date de fin sejour,
 * le nombre d'enfants,
 * le nombre d'adultes,
 * le nom et prenom du client concatené,
 * le numero de la chambre,
 * la date de reservation.
 * @author akimsoule
 *
 */
public class ReservationAudit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Le login qui a fait la reservation.
	 */
	private String acteur;
	/**
	 * Le type d'update (CREATION ou SUPPRESSION).
	 */
	private String type_update;
	/**
	 * La date de debut sejour.
	 */
	private String debut_audit;
	/**
	 * La date de fin sejour.
	 */
	private String fin_audit;
	/**
	 * Le nombre d'enfant.
	 */
	private int nombre_enfant_audit;
	/**
	 * Le nombre d'adultes.
	 */
	private int nombre_adulte_audit;
	/**
	 * Le nom et le prenom concatés du client.
	 */
	private String nom_prenom_client_audit;
	/**
	 * Le numero de la chambre.
	 */
	private int chambre_audit;
	/**
	 * Le type d'update (CREATION ou SUPPRESSION).
	 */
	private String date_de_update;

	/**
	 * Principal constructeur de la classe.
	 * @param acteur
	 * l'acteur qui cree, supprime ou modifie la reservation.
	 * @param type_update
	 * le type d'update (CREATION, SUPPRESSION ou MODIFICATION).
	 * @param debut_audit
	 * La date de debut reservation.
	 * @param fin_audit
	 * La date de fin reservation.
	 * @param nombre_enfant_audit
	 * Le nombre d'enfants
	 * @param nombre_adulte_audit
	 * Le nombre d'adultes.
	 * @param nom_prenom_client_audit
	 * Le nom et prenom concatene du client.
	 * @param chambre_audit
	 * Le numero de la chambre.
	 * @param date_de_update
	 * La date de l'update.
	 */
	public ReservationAudit(String acteur, String type_update, String debut_audit, String fin_audit,
			int nombre_enfant_audit, int nombre_adulte_audit, String nom_prenom_client_audit, int chambre_audit,
			String date_de_update) {
		super();
		this.acteur = acteur;
		this.type_update = type_update;
		this.debut_audit = debut_audit;
		this.fin_audit = fin_audit;
		this.nombre_enfant_audit = nombre_enfant_audit;
		this.nombre_adulte_audit = nombre_adulte_audit;
		this.nom_prenom_client_audit = nom_prenom_client_audit;
		this.chambre_audit = chambre_audit;
		this.date_de_update = date_de_update;
	}

	public String getActeur() {
		return acteur;
	}

	public void setActeur(String acteur) {
		this.acteur = acteur;
	}

	public String getType_update() {
		return type_update;
	}

	public void setType_update(String type_update) {
		this.type_update = type_update;
	}

	public String getDebut_audit() {
		return debut_audit;
	}

	public void setDebut_audit(String debut_audit) {
		this.debut_audit = debut_audit;
	}

	public String getFin_audit() {
		return fin_audit;
	}

	public void setFin_audit(String fin_audit) {
		this.fin_audit = fin_audit;
	}

	public int getNombre_enfant_audit() {
		return nombre_enfant_audit;
	}

	public void setNombre_enfant_audit(int nombre_enfant_audit) {
		this.nombre_enfant_audit = nombre_enfant_audit;
	}

	public int getNombre_adulte_audit() {
		return nombre_adulte_audit;
	}

	public void setNombre_adulte_audit(int nombre_adulte_audit) {
		this.nombre_adulte_audit = nombre_adulte_audit;
	}

	public String getNom_prenom_client_audit() {
		return nom_prenom_client_audit;
	}

	public void setNom_prenom_client_audit(String nom_prenom_client_audit) {
		this.nom_prenom_client_audit = nom_prenom_client_audit;
	}

	public int getChambre_audit() {
		return chambre_audit;
	}

	public void setChambre_audit(int chambre_audit) {
		this.chambre_audit = chambre_audit;
	}

	public String getDate_de_update() {
		return date_de_update;
	}

	public void setDate_de_update(String date_de_update) {
		this.date_de_update = date_de_update;
	}

	

}
