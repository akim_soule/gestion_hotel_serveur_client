package Modele.objet.classe_java;

import java.io.Serializable;

/**
 * Classe permettant de representer un employe suspendu.
 * Elle a pour principals attributs l'id de l'employe et le login de l'employe.
 * @author akimsoule
 *
 */
public class EmployeSuspendu implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * L'id de l'employe.
	 */
	private int id;
	/**
	 * Le login de l'employe.
	 */
	private String login;

	/**
	 * Principal constructeur de la classe.
	 * @param id
	 * l'id de l'employe.
	 * @param login
	 * le login de l'employe.
	 */
	public EmployeSuspendu(int id, String login) {
		this.id = id;
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return id + ", " + login;
	}
	
	

}
