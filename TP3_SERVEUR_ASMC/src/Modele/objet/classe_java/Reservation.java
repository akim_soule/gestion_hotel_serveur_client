package Modele.objet.classe_java;

import java.io.Serializable;


public class Reservation implements Serializable {

	
	private static final long serialVersionUID = 1L;
	/**
	 * L'id de reservation.
	 */
	private int id;
	/**
	 * Le nom du client.
	 */
	private String nom;
	/**
	 * Le prenom du client.
	 */
	private String prenom;
	/**
	 * La date du debut sejour.
	 */
	private String dateDebutSejour;
	/**
	 * La date de fin sejour.
	 */
	private String dateFinSejour;
	/**
	 * L'id de l'employe qui a fait la reservation.
	 */
	private int id_employe;
	/**
	 * L'id du client.
	 */
	private int id_client;
	/**
	 * Le nombre d'enfants
	 */
	private int nombre_enfant;
	/**
	 * Le nombre d'adulte.
	 */
	private int nombre_adulte;

	/**
	 * Le numero de la chambre.
	 */
	private int chambre;

	/**
	 * Le constructeur de la classe.
	 * @param id
	 * l'id de la reservation.
	 * @param nom
	 * le nom du client.
	 * @param prenom
	 * le prenom du client.
	 * @param dateDebutSejour
	 * la date de debut sejour.
	 * @param dateFinSejour
	 * la date de fin sejour.
	 * @param nombre_enfant
	 * le nombre d'enfants.
	 * @param nombre_adulte
	 * le nombre d'adultes.
	 * @param pid_employe
	 * l'id de l'employe.
	 * @param pid_client
	 * l'id du client.
	 * @param chambre
	 * le numero de la chambre.
	 */
	public Reservation(int id, String nom, String prenom, String dateDebutSejour, String dateFinSejour,
			int nombre_enfant, int nombre_adulte, int pid_employe, int pid_client, int chambre) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.dateDebutSejour = dateDebutSejour;
		this.dateFinSejour = dateFinSejour;
		this.nombre_enfant = nombre_enfant;
		this.nombre_adulte = nombre_adulte;
		this.id_employe = pid_employe;
		this.id_client = pid_client;
		this.chambre = chambre;
	}

	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_employe() {
		return id_employe;
	}

	public void setId_employe(int id_employe) {
		this.id_employe = id_employe;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDateDebutSejour() {
		return dateDebutSejour;
	}

	public void setDateDebutSejour(String dateDebutSejour) {
		this.dateDebutSejour = dateDebutSejour;
	}

	public String getDateFinSejour() {
		return dateFinSejour;
	}

	public void setDateFinSejour(String dateFinSejour) {
		this.dateFinSejour = dateFinSejour;
	}

	public int getNombre_enfant() {
		return nombre_enfant;
	}

	public void setNombre_enfant(int nombre_enfant) {
		this.nombre_enfant = nombre_enfant;
	}

	public int getNombre_adulte() {
		return nombre_adulte;
	}

	public void setNombre_adulte(int nombre_adulte) {
		this.nombre_adulte = nombre_adulte;
	}

	public int getChambre() {
		return chambre;
	}

	public void setChambre(int chambre) {
		this.chambre = chambre;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateDebutSejour=" + dateDebutSejour
				+ ", dateFinSejour=" + dateFinSejour + ", nombre_enfant=" + nombre_enfant + ", nombre_adulte="
				+ nombre_adulte + ", chambre=" + chambre + "]";
	}

	

}
