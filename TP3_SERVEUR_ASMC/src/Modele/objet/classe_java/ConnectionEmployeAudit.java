package Modele.objet.classe_java;

import java.io.Serializable;

/**
 * Classe representant l'audit des connections des enployes.
 * Elle a pour principals attributs le login de l'employe, un string (connection ou deconnection a inserer dans la table) et la date.
 * @author akimsoule
 *
 */
public class ConnectionEmployeAudit  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Le login de l'employe.
	 */
	private String login_employe;
	/**
	 * Le String de la table (CONNECTION et DECONNECTION).
	 */
	private String connection_audit;
	/**
	 * La date d'enregistrement.
	 */
	private String localDateTime;
	
	/**
	 * Le constructeur de la classe.
	 * @param login_employe
	 * le login de l'employe.
	 * @param connection_audit
	 * la chaine de caracteres (CONNECTED ou DECONNECTED).
	 * @param localDateTime
	 * la date.
	 */
	public ConnectionEmployeAudit(String login_employe, String connection_audit, String localDateTime) {
		super();
		this.login_employe = login_employe;
		this.connection_audit = connection_audit;
		this.localDateTime = localDateTime;
	}

	public String getLogin_employe() {
		return login_employe;
	}

	public void setLogin_employe(String login_employe) {
		this.login_employe = login_employe;
	}

	public String getConnection_audit() {
		return connection_audit;
	}

	public void setConnection_audit(String connection_audit) {
		this.connection_audit = connection_audit;
	}

	public String getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(String localDateTime) {
		this.localDateTime = localDateTime;
	}

	
	@Override
	public String toString() {
		return "ConnectionEmployeAudit [login_employe=" + login_employe + ", connection_audit=" + connection_audit
				+ ", localDateTime=" + localDateTime + "]";
	}
	

	
	
	
	
	

}
