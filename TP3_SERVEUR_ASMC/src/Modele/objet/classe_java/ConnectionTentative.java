package Modele.objet.classe_java;

/**
 * Classe representant une tentative de connection.
 * Elle a pour principals attributs l'id de la connection tentative, 
 * le login qui a essaye de se connecter et le nombre de tentatives.
 * @author akimsoule
 *
 */
public class ConnectionTentative {
	
	/**
	 * L'id de la connection tentative.
	 */
	private int id_connection_tentative;
	/**
	 * Le login qui a essaye de se connecter.
	 */
	private String login_connection_tentative;
	/**
	 * Le nombre de connections.
	 */
	private int nombre_connection_tentative;
	
	/**
	 * Principal constructeur de la classe.
	 * @param id_connection_tentative
	 * l'id de la connection tentative.
	 * @param login_connection_tentative
	 * le login de celui qui a essaye de se connecter.
	 * @param nombre_connection_tentative
	 * le nombre de tentatives connection.
	 */
	public ConnectionTentative(int id_connection_tentative, String login_connection_tentative,
			int nombre_connection_tentative) {
		this.id_connection_tentative = id_connection_tentative;
		this.login_connection_tentative = login_connection_tentative;
		this.nombre_connection_tentative = nombre_connection_tentative;
	}

	public synchronized int getId_connection_tentative() {
		return id_connection_tentative;
	}

	public synchronized void setId_connection_tentative(int id_connection_tentative) {
		this.id_connection_tentative = id_connection_tentative;
	}

	public synchronized String getLogin_connection_tentative() {
		return login_connection_tentative;
	}

	public synchronized void setLogin_connection_tentative(String login_connection_tentative) {
		this.login_connection_tentative = login_connection_tentative;
	}

	public synchronized int getNombre_connection_tentative() {
		return nombre_connection_tentative;
	}

	public synchronized void setNombre_connection_tentative(int nombre_connection_tentative) {
		this.nombre_connection_tentative = nombre_connection_tentative;
	}
	
	
	
	

}
