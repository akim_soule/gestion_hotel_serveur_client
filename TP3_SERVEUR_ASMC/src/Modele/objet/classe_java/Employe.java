package Modele.objet.classe_java;

import java.io.Serializable;

/**
 * Classe representant une classe Employe.
 * Elle a pour principals attributs l'id de l'employe et le login.
 * @author akimsoule
 *
 */
public class Employe implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * L'id de l'employe.
	 */
	private int id_employe;
	/**
	 * Le login de l'employe.
	 */
	private String login_employe;

	/**
	 * Principal constructeur de la classe.
	 * @param id_employe
	 * l'id de l'employe.
	 * @param login_employe
	 * le login de l'employe.
	 */
	public Employe(int id_employe, String login_employe) {
		super();
		this.id_employe = id_employe;
		this.login_employe = login_employe;

	}

	public int getId_employe() {
		return id_employe;
	}

	public void setId_employe(int id_employe) {
		this.id_employe = id_employe;
	}

	public String getLogin_employe() {
		return login_employe;
	}

	public void setLogin_employe(String login_employe) {
		this.login_employe = login_employe;
	}

	@Override
	public String toString() {
		return id_employe + ", " + login_employe;
	}

	

}
