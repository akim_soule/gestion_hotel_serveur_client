package Modele.instance.connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe representant une connection a la base de donnees.
 * Elle a pour attributs l'url, le user, le password et l'objet connection.
 * @author akimsoule
 *
 */
public class ConnectionModele {

	/**
	 * l'url de connection.
	 */
	private static String url = (System.getProperty("os.name").toLowerCase().contains("win"))
			? "jdbc:mysql://127.0.0.1/gestion_hotel"
			: "jdbc:mysql://127.0.0.1:8889/gestion_hotel";

	/**
	 * le user root
	 */
	private String user = "root";

	/**
	 * le password root
	 */
	private String passwd = "root";

	/**
	 * l'objet connection
	 */
	private static Connection connect;

	/**
	 * Constructeur de la classe
	 */
	private ConnectionModele() {
		try {
			connect = DriverManager.getConnection(url, user, passwd);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Recuperation de l'instance de la connection.
	 * @return
	 * l'objet connection.
	 */
	public static Connection getInstance() {
		if (connect == null) {
			new ConnectionModele();
		}
		return connect;
	}
}