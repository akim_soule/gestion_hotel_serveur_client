package Modele.package_utilitaire;

import java.util.List;
import java.util.logging.Level;

import Modele.objet.classe_java.Chambre;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe permettant de traiter les dates.
 * @author akimsoule
 *
 */
public class TraitementDate {
	
	/**
	 * Methode permettant de recuperer l'annee.
	 * @param string
	 * La parametre recupere de la base de donnees
	 * @return
	 * l'annee
	 */
	public static synchronized int getAnnee(String string) {
		int i = 0;
		try {
			i = Integer.parseInt(string.substring(0, 4));
		} catch (Exception e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return i;
	}
	
	/**
	 * Methode permettant de recuperer le mois.
	 * @param string
	 * La parametre recupere de la base de donnees
	 * @return
	 * le mois
	 */
	public static synchronized int getMois(String string) {
		int i = 0;
		try {
			i = Integer.parseInt(string.substring(4, 6));
		} catch (Exception e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return i;
	}
	
	/**
	 * Methode permettant de recuperer le jour.
	 * @param string
	 * La parametre recupere de la base de donnees
	 * @return
	 * le jour.
	 */
	public static synchronized int getJour(String string) {
		int i = 0;
		try {
			i = Integer.parseInt(string.substring(6, 8));
		} catch (Exception e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return i;
	}
	
	/**
	 * Methode permettant de recuperer l'heure.
	 * @param string
	 * La parametre recupere de la base de donnees
	 * @return
	 * l'heure
	 */
	public static synchronized int getHeure(String string) {
		int i = 0;
		try {
			i = Integer.parseInt(string.substring(8, 10));
		} catch (Exception e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return i;
	}
	
	/**
	 * Methode permettant de recuperer la minute.
	 * @param string
	 * La parametre recupere de la base de donnees
	 * @return
	 * la minute.
	 */
	public static synchronized int getMinute(String string) {
		int i = 0;
		try {
			i = Integer.parseInt(string.substring(10, 12));
		} catch (Exception e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return i;
	}
	
	/**
	 * Methode permettant de recuperer la seconde.
	 * @param string
	 * La parametre recupere de la base de donnees
	 * @return
	 * la seconde.
	 */
	public static synchronized int getSeconde(String string) {
		int i = 0;
		try {
			i = Integer.parseInt(string.substring(12, 14));
		} catch (Exception e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return i;
	}
	
	/**
	 * Methode permettant de verifier la disponibilite d'une chambre.
	 * @param listChambresPrise
	 * La liste des chambres prises.
	 * @param chambre1
	 * La chambre a ajouter.
	 * @return
	 * vrai si la chambre a disponible et faux sinon.
	 */
	public static synchronized boolean verificationPossibilite(List<Chambre> listChambresPrise, Chambre chambre1) {
		boolean retour = false;
		if (!listChambresPrise.contains(chambre1)) {
			retour = true;
		}else {
			for (int i = 0 ; i < listChambresPrise.size(); i++) {
				if (chambre1.equals(listChambresPrise.get(i))){
					if (estCeQueLesPeriodesSeChevauchent(chambre1, listChambresPrise.get(i))) {
						System.out.println("La chambre "+chambre1+" ne peut pas etre ajoutee");
						retour = false;
						break;
					}else {
						retour = true;
					}
				}
			}
		}
		
		return retour;
		
	}

	/**
	 * Methode qui verifie si deux periodes se chevauchent.
	 * @param chambreAjouter
	 * la chambre a ajouter.
	 * @param chambre2
	 * la chambre avec laquelle comparer.
	 * @return 
	 * vrai si les periodes se chevauchent et faux sinon.
	 */
	public static synchronized boolean estCeQueLesPeriodesSeChevauchent(Chambre chambreAjouter, Chambre chambre2){
		boolean retour = false;
		
		if (chambreAjouter.getLocalDateTimeDebut().isAfter(chambre2.getLocalDateTimeDebut()) && 
				chambreAjouter.getLocalDateTimeFin().isAfter(chambre2.getLocalDateTimeDebut()) ||
				
				chambre2.getLocalDateTimeDebut().isAfter(chambreAjouter.getLocalDateTimeDebut()) && 
					chambre2.getLocalDateTimeFin().isAfter(chambreAjouter.getLocalDateTimeDebut())){
			retour = false;
		}else {
			retour = true;
		}
//		if (chambreAjouter.equalsComplet(chambre2)) {
//			retour = true;
//		}
//		if (chambreAjouter.getLocalDateTimeFin().compareTo(chambre2.getLocalDateTimeDebut()) == 0) {
//			retour = true;
//		}
//		if (chambreAjouter.getLocalDateTimeDebut().compareTo(chambre2.getLocalDateTimeFin()) == 0) {
//			retour = true;
//		}
//		if (chambreAjouter.getLocalDateTimeDebut().compareTo(chambre2.getLocalDateTimeDebut()) > 1 &&
//				chambreAjouter.getLocalDateTimeFin().compareTo(chambre2.getLocalDateTimeDebut()) < 1) {
//			retour = true;
//		}
//		if (chambre2.getLocalDateTimeDebut().compareTo(chambreAjouter.getLocalDateTimeDebut()) > 1 &&
//				chambre2.getLocalDateTimeFin().compareTo(chambreAjouter.getLocalDateTimeDebut()) < 1) {
//			retour = true;
//		}
//		if (chambreAjouter.getLocalDateTimeDebut().compareTo(chambre2.getLocalDateTimeDebut()) > 1 &&
//				chambreAjouter.getLocalDateTimeDebut().compareTo(chambre2.getLocalDateTimeFin()) < 1) {
//			retour = true;
//		}
//		if (chambreAjouter.getLocalDateTimeFin().compareTo(chambre2.getLocalDateTimeDebut()) > 1 &&
//				chambreAjouter.getLocalDateTimeFin().compareTo(chambre2.getLocalDateTimeFin()) < 1) {
//			retour = true;
//		}
	
		return retour;	
	}

}
