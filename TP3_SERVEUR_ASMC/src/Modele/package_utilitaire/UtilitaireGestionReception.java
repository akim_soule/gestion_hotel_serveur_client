package Modele.package_utilitaire;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import Modele.ThreadTraitementClient;
import Modele.Donnees.DonneesServeur;
import Modele.classe_abstraite_dao.DAOFactory;
import Modele.objet.classe_java.Chambre;
import Modele.objet.classe_java.Reservation;

/**
 * Classe permettant de gerer la reception des messages sur le serveur.
 * @author akimsoule
 *
 */
public class UtilitaireGestionReception {

	/**
	 * Methode permettant de gerer le login.
	 * @param modeConnecte
	 * le boolean indiquant si le client est connecte ou pas.
	 * @param response
	 * la reponse recu sur le serveur
	 * @param loginEmploye
	 * le login de l'employe
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 * @return
	 * la reponse du serveur vrai si la connection s'est realisee et faux dans le cas contraire.
	 */
	public synchronized boolean gererLogin(boolean modeConnecte, String response, String loginEmploye,
			ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		if (modeConnecte == true) {
			aEnvoyer = "Vous etes connectes deja !";

		} else {

			String login = response.split("-")[1];
			String passwordHashe = response.split("-")[2];
			// String salt = "xAKqWeJo2fIDmfYlrIwR7";
			boolean verifieSiLeUserExiste = DAOFactory.getEmployeDAO().verifierSiUnEmployeExiste(login);
			boolean verifierSiLeUserAEteSuspendu = DAOFactory.getEmployeSuspenduDAO()
					.verifierSiUnEmployeEstSupendu(login);
			if (verifieSiLeUserExiste) {
				if (verifierSiLeUserAEteSuspendu) {
					aEnvoyer = "CV-Vous avez ete suspendu.";
					modeConnecte = false;
				} else {
					if (DAOFactory.getEmployeDAO().estCeQueLeMotDePasseEstLeBon(login, passwordHashe)) {
						loginEmploye = login;
						DAOFactory.getConnectionEmployeAuditDAO().insertInto_Connection_employe_audit("CONNECTED",
								login);
						DAOFactory.getConnectionTentative().deleteUser(login);
						DonneesServeur.lireobserListeConnectionEmployeAudits();
						if (login.toUpperCase().trim().equals("ADMIN")) {
							aEnvoyer = "CV-ADMIN_CONNECTED";
						} else {
							aEnvoyer = "CV-CONNECTED";
						}
						modeConnecte = true;
					} else {
						modeConnecte = false;
						if (!login.toUpperCase().trim().equals("ADMIN")) {
							aEnvoyer = "CV-Mauvais identifiant ou mot de passe. Il vous reste ";
							if (DAOFactory.getConnectionTentative().incrementerNombre(login)) {
								aEnvoyer += "" + (4 - DAOFactory.getConnectionTentative().getTentative(login))
										+ " tentatives";
							} else {
								DAOFactory.getEmployeSuspenduDAO().insertInto_Employe_suspendu(login);
								aEnvoyer = "CV-Vous avez ete suspendu ";

							}
						}
					}
				}

			} else {
				modeConnecte = false;
				aEnvoyer = "CV-Mauvais identifiant ou mot de passe";
			}

			traitementClient.envoyer(aEnvoyer);

		}
		return modeConnecte;

	}

	/**
	 * Methode permettant du supprimer un employe suspendu
	 * @param response
	 * la reponse recu sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererSuppressionEmployeSuspendu(String response,
			ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";

		String loginASupprimer = response.split("-")[1];

		if (DAOFactory.getEmployeSuspenduDAO().supprimerEmploye(loginASupprimer)) {
			aEnvoyer = "CVAUDIT-Employe suspendu suprimé avec succes !";
			traitementClient.envoyerObjet();
		} else {
			aEnvoyer = "CVAUDIT-Employe suspendu suppression impossible ";
		}
		traitementClient.envoyer(aEnvoyer);

	}

	/**
	 * Methode permettant de supprimer un employe.
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererSuppressionEmploye(String response, ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		String loginASupprimer = response.split("-")[1];

		if (DAOFactory.getEmployeDAO().supprimerEmploye(loginASupprimer)) {
			aEnvoyer = "CVAUDIT-Employe suprimé avec succes !";
			traitementClient.envoyerObjet();
		} else {
			aEnvoyer = "CVAUDIT-Employe suppression impossible ";
		}
		traitementClient.envoyer(aEnvoyer);

	}

	/**
	 * Methode permettant de creer un employe
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererCreeEmploye(String response, ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		String nouveauLogin = response.split("-")[1];
		String nouveauPassword = response.split("-")[2];

		if (DAOFactory.getEmployeDAO().insertIntoEmploye(nouveauLogin, nouveauPassword)) {
			aEnvoyer = "CVAUDIT-Employe enregistré avec succes !";
			traitementClient.envoyer(aEnvoyer);
			traitementClient.envoyerObjet();
		} else {
			aEnvoyer = "CVAUDIT-Employe existe deja !";
		}
		traitementClient.envoyer(aEnvoyer);

	}

	/**
	 * Methode permettant de creer un client.
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererCreeClient(String response, ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		String nom = response.split("-")[1];
		String prenom = response.split("-")[2];
		if (!DAOFactory.getClientDAO().verifierSiLeCLientExiste(nom, prenom)) {
			DAOFactory.getClientDAO().insertIntoClient(nom, prenom);
			aEnvoyer = "CVUEGES-Client enregistre avec succes !";
			traitementClient.envoyerObjet();

		} else {
			aEnvoyer = "CVUEGES-le client existe deja";
		}

		traitementClient.envoyer(aEnvoyer);

	}

	/**
	 * Methode permettant de gerer la suppression d'un client.
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererSuppressionClient(String response, ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		
		String nom = response.split("-")[1];
		String prenom = response.split("-")[2];
		System.out.println(nom + prenom);
		if (DAOFactory.getClientDAO().supprimerClient(nom, prenom)) {
			aEnvoyer = "CVUEGES-Client supprimé avec succes";
		} else {
			aEnvoyer = "CVUEGES-Impossible de supprimer le client";
		}
		traitementClient.envoyer(aEnvoyer);
		traitementClient.envoyerObjet();
		
	}

	/**
	 * Methode permettant de gerer la suppression d'une reservation.
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererSuppressionReservation(String response,
			ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		int id = Integer.parseInt(response.split("/")[1]);
		int numeroChambre = Integer.parseInt(response.split("/")[2]);
		String dateDebut = response.split("/")[3].replaceAll("-", "").replaceAll(":", "")
				.replaceAll(" ", "").trim();
		String dateFin = response.split("/")[4].replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "")
				.trim();
		String userNom = response.split("/")[5];
		int nombre_adulte = Integer.parseInt(response.split("/")[6]);
		int nombre_enfant = Integer.parseInt(response.split("/")[7]);

		Chambre chambreASupprimer = new Chambre(numeroChambre);
		chambreASupprimer.setDateTimeDebutEtFin(dateDebut, dateFin);

		DAOFactory.getReservationDAO().supprimerReservation(id, userNom, dateDebut, dateFin, nombre_enfant,
				nombre_adulte, chambreASupprimer.getNumero());
		aEnvoyer = "CVUEGES-Réservation supprimée avec succes";
		traitementClient.envoyer(aEnvoyer);
		traitementClient.envoyerObjet();
		
		
		
	}

	/**
	 * Methode permettant de gerer la modification d'un client.
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererModifierClient(String response, ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		if (response.split("-").length > 1) {
			String nom = response.split("-")[1];
			String prenom = response.split("-")[2];
			String ancienNom = nom.split("/")[1];
			String nouveauNom = nom.split("/")[0];
			String ancienPrenom = prenom.split("/")[1];
			String nouveauPrenom = prenom.split("/")[0];
			DAOFactory.getClientDAO().mettreAJourClient(nouveauNom, nouveauPrenom, ancienNom, ancienPrenom);
			aEnvoyer = "CVUEGES-Mise a jour du client effectue !";
			traitementClient.envoyer(aEnvoyer);
			traitementClient.envoyerObjet();

		}
		
	}

	/**
	 * Methode permettant de gerer la creation d'une reservation.
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererCreeReservation(String response, ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		String login = response.split("/")[1].trim();

		int idClient = Integer.parseInt(response.split("/")[2].trim());
		String nomClient = response.split("/")[3].trim();
		String prenomClient = response.split("/")[4].trim();
		String dateDebut = response.split("/")[5].trim();
		String dateFin = response.split("/")[6].trim();
		int numeroChambre = Integer.parseInt(response.split("/")[7].trim());
		int nombreAdulte = Integer.parseInt(response.split("/")[8].trim());
		int nombreEnfant = Integer.parseInt(response.split("/")[9].trim());

		Chambre chambreAjouter = new Chambre(numeroChambre);

		chambreAjouter.setDateTimeDebutEtFin(dateDebut, dateFin);
		
		if (TraitementDate.verificationPossibilite(
				DonneesServeur.obserListChambrePrises.stream().collect(Collectors.toList()),
				chambreAjouter)) {
			DAOFactory.getReservationDAO().faireUnereservation(numeroChambre, idClient, dateFin, dateDebut,
					login, nomClient, prenomClient, nombreAdulte, nombreEnfant);
			aEnvoyer = "CVUEGES-Reservation effectue !";
			traitementClient.envoyerObjet();

		} else {
			aEnvoyer = "CVUEGES-Chambre indisponible pour la periode ";
		}
		
		traitementClient.envoyer(aEnvoyer);
		
	}

	/**
	 * Methode permettant de modifier une reservation.
	 * @param response
	 * la reponse recue sur le serveur.
	 * @param traitementClient
	 * l'objet du ThreadTraitementClient permettant d'envoyer des objets au client.
	 */
	public synchronized void gererModifierReservation(String response, ThreadTraitementClient traitementClient) {
		String aEnvoyer = "";
		int id_reservation = Integer.parseInt(response.split("/")[1].trim());
		String dateDebut = response.split("/")[2].trim();
		String dateFin = response.split("/")[3].trim();
		int numeroChambre = Integer.parseInt(response.split("/")[4].trim());
		int nombreAdulte = Integer.parseInt(response.split("/")[5].trim());
		int nombreEnfant = Integer.parseInt(response.split("/")[6].trim());
		String userNom = response.split("/")[7].trim();
		String nomPrenomClient = response.split("/")[8].trim();
		DonneesServeur.lire();
		Reservation reservation = null;
		List<Reservation> listReservations = DonneesServeur.obserListReservations.stream().collect(Collectors.toList());
		for (int i = 0; i < listReservations.size(); i++) {
			if (listReservations.get(i).getId() == id_reservation) {
				reservation = listReservations.get(i);
				break;
			}
		}
		
		Chambre ancienneChambre = new Chambre(reservation.getChambre());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime debut = LocalDateTime.parse(reservation.getDateDebutSejour(), formatter);
		LocalDateTime fin = LocalDateTime.parse(reservation.getDateFinSejour(), formatter);
		
		ancienneChambre.setLocalDateTimeDebut(debut);
		ancienneChambre.setLocalDateTimeFin(fin);
		
		Chambre chambreAModifier = new Chambre(numeroChambre);
		chambreAModifier.setDateTimeDebutEtFin(dateDebut, dateFin);
		List<Chambre> listChambreAConsiderer = DonneesServeur.obserListChambrePrises.stream().collect(Collectors.toList());
		Iterator<Chambre> it = listChambreAConsiderer.iterator();
		while(it.hasNext()) {
			Chambre cham = it.next();
			if (cham.equalsComplet(ancienneChambre)) {
				it.remove();
				break;
			}
		}
	
		if (TraitementDate.verificationPossibilite(
				listChambreAConsiderer,
				chambreAModifier)) {
			DAOFactory.getReservationDAO().modifierReservation(id_reservation, dateDebut, dateFin,
					numeroChambre, nombreAdulte, nombreEnfant, userNom, nomPrenomClient);
			aEnvoyer = "CVUEGES-Reservation modifiée avec succes";
			
			traitementClient.envoyerObjet();
			
		}else {
			aEnvoyer = "CVUEGES-Chambre a modifier indisponible pour la periode ";
		}
		
		traitementClient.envoyer(aEnvoyer);
		
	}

}
