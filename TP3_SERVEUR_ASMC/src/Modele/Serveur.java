package Modele;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.logging.Level;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

import Modele.Donnees.DonneesServeur;
import Modele.package_logger.MyLoggingConnectionIPetPort;
import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe representant un serveur.
 * @author akimsoule
 *
 */
public class Serveur extends Thread {
	private int port = 2345;
	private SSLServerSocket server = null;
	private String ksName = "ressources/nomdelacle3.jks"; //cle prive
	private char ksPass[] = "qwerty".toCharArray();
	private char ctPass[] = "qwerty".toCharArray();
	public Serveur() {
		
			//retourne l'objet KeyStore avec le type de clé spécifié, ici clé propriétaire de Sun
			
			try {
				KeyStore ks = KeyStore.getInstance("JKS");
				//charge le fichier
				ks.load(new FileInputStream(ksName), ksPass);

				//La structure des certificats est normalisée par le standard X.509 de l'UIT (plus exactement X.509v3), qui définit les informations contenues dans le certificat :
				KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
				kmf.init(ks, ctPass);

				// Type de protocole utilisé
				SSLContext sc = SSLContext.getInstance("TLS");
				sc.init(kmf.getKeyManagers(), null, null);

				// Récupération du fabriquant de sockets serveurs SSLs
				SSLServerSocketFactory ssf = sc.getServerSocketFactory();
				SSLServerSocket s = (SSLServerSocket) ssf.createServerSocket(port);
				this.server = s;
				
				DonneesServeur.executeExecutor();
				
			} catch (KeyStoreException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			} catch (CertificateException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			} catch (UnrecoverableKeyException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			} catch (KeyManagementException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			}	
	}
	public void run() {
		while (true) {
			try {
				System.out.println("Serveur en attente");
				SSLSocket socketCourant = (SSLSocket) server.accept();
				
				System.out.println("Connection cliente recue");
				MyLoggingConnectionIPetPort.log(Level.INFO, "Conection recue du "+socketCourant.getInetAddress().toString()+"-----"+socketCourant.getPort());
				ThreadTraitementClient traitement = new ThreadTraitementClient(socketCourant);
				traitement.start();
			} catch (IOException e) {
				e.printStackTrace();
				MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
			}
		}
	}

}
