package Modele.classe_abstraite_dao;

import java.sql.Connection;

import Modele.instance.connection.ConnectionModele;
import Modele.objet.classe_dao.ChambreDAO;
import Modele.objet.classe_dao.ClientDAO;
import Modele.objet.classe_dao.ConnectionEmployeAuditDAO;
import Modele.objet.classe_dao.ConnectionTentativeDAO;
import Modele.objet.classe_dao.EmployeDAO;
import Modele.objet.classe_dao.EmployeSuspenduDAO;
import Modele.objet.classe_dao.HistoriqueSuspensionDAO;
import Modele.objet.classe_dao.ReservationAuditDAO;
import Modele.objet.classe_dao.ReservationDAO;

/**
 * Classe permettant de recuperer des objets servant a interagir avec la bd.
 * @author akimsoule
 *
 */
public class DAOFactory {
	protected static final Connection conn = ConnectionModele.getInstance();

	/**
	 * @return
	 * l'objet ClientDAO.
	 */
	public static ClientDAO getClientDAO() {
		return new ClientDAO(conn);
	}

	/**
	 * @return
	 * l'objet ChambreDAO.
	 */
	public static ChambreDAO getChambreDAO() {
		return new ChambreDAO(conn);
	}

	/**
	 * @return
	 * l'objet EmployeDAO.
	 */
	public static EmployeDAO getEmployeDAO() {
		return new EmployeDAO(conn);
	}

	/**
	 * @return
	 * l'objet ReservationDAO.
	 */
	public static ReservationDAO getReservationDAO() {
		return new ReservationDAO(conn);
	}

	/**
	 * @return
	 * l'objet ReservationAuditDAO.
	 */
	public static ReservationAuditDAO getReservationAuditDAO() {
		return new ReservationAuditDAO(conn);
	}
	
	/**
	 * @return
	 * l'objet ConnectionEmployeAuditDAO.
	 */
	public static ConnectionEmployeAuditDAO getConnectionEmployeAuditDAO() {
		return new ConnectionEmployeAuditDAO(conn);
	}

	/**
	 * @return
	 * l'objet EmployeSuspenduDAO.
	 */
	public static EmployeSuspenduDAO getEmployeSuspenduDAO() {
		return new EmployeSuspenduDAO(conn);
	}

	/**
	 * @return
	 * l'objet HistoriqueSuspensionDAO.
	 */
	public static HistoriqueSuspensionDAO getHistoriqueSuspensionDAO() {
		return new HistoriqueSuspensionDAO(conn);
	}

	/**
	 * @return
	 * l'objet ConnectionTentativeDAO.
	 */
	public static ConnectionTentativeDAO getConnectionTentative() {
		return new ConnectionTentativeDAO(conn);
	}
}
