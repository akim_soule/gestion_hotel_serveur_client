package Modele.Donnees;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import Modele.classe_abstraite_dao.DAOFactory;
import Modele.instance.connection.ConnectionModele;
import Modele.objet.classe_java.Chambre;
import Modele.objet.classe_java.ClientHotel;
import Modele.objet.classe_java.ConnectionEmployeAudit;
import Modele.objet.classe_java.Employe;
import Modele.objet.classe_java.EmployeSuspendu;
import Modele.objet.classe_java.HistoriqueSuspension;
import Modele.objet.classe_java.Reservation;
import Modele.objet.classe_java.ReservationAudit;
import Modele.package_logger.MyLoggingMessageErreur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Classe permettant de representer les donnees du serveur. Elle a pour
 * principals attributs des observableList d'employes, des reservations, la
 * liste de reservation, la liste des chambres prises, la liste des objets
 * connectionEmployeAudits, la liste des obbjets reservationAudits, la liste des
 * objets historique employe audits, la liste des employes suspendus.
 * 
 * @author akimsoule
 *
 */
public class DonneesServeur {

	/**
	 * La liste des employes.
	 */
	public static ObservableList<Employe> obserListEmployes = FXCollections.observableArrayList();
	/**
	 * La liste des clients de l'hotel.
	 */
	public static ObservableList<ClientHotel> obserListClientHotels = FXCollections.observableArrayList();
	/**
	 * La liste des reservations.
	 */
	public static ObservableList<Reservation> obserListReservations = FXCollections.observableArrayList();
	/**
	 * La liste des chambres prises.
	 */
	public static ObservableList<Chambre> obserListChambrePrises = FXCollections.observableArrayList();
	/**
	 * La liste des objets connection employe audits.
	 */
	public static ObservableList<ConnectionEmployeAudit> obserListConnectionEmployeAudits = FXCollections
			.observableArrayList();
	/**
	 * La liste des objets reservation audits.
	 */
	public static ObservableList<ReservationAudit> obserListReservationAudits = FXCollections.observableArrayList();
	/**
	 * La liste des objets historique suspension.
	 */
	public static ObservableList<HistoriqueSuspension> obserListHistoriqueSuspensions = FXCollections
			.observableArrayList();
	/**
	 * La liste des employes suspendus.
	 */
	public static ObservableList<EmployeSuspendu> obserListEmployeSuspendus = FXCollections.observableArrayList();

	/**
	 * Constructeur de la classe permettant d'initialiser les donnees.
	 */
	public DonneesServeur() {
		initiliaze();
	}

	/**
	 * Methode permettant d'appeler la methode lire.
	 */
	private synchronized void initiliaze() {
		lire();
	}

	/**
	 * Methode permettant de lire la base de donnees.
	 */
	public synchronized static void lire() {
		lireobserListeDesEmployes();
		lireobserListeDesClients();
		lireobserListeDesReservations();
		lireobserListeChambrePrises();
		lireobserListeConnectionEmployeAudits();
		lireobserListeReservationAudits();
		lireobserListeHistoriqueSuspensions();
		lireobserListeEmployeSuspendus();
	}

	/**
	 * Methode permettant de lire la liste des employes suspendus.
	 */
	public synchronized static void lireobserListeEmployeSuspendus() {
		obserListEmployeSuspendus.setAll(DAOFactory.getEmployeSuspenduDAO().consulterBDEmployesSuspendus());
		lireobserListeHistoriqueSuspensions();
		// envoyer(new obserListEmployeSuspendu(obserListEmployeSuspendus));
	}

	/**
	 * Methode permettant de lire la liste des historiques suspensions.
	 */
	public synchronized static void lireobserListeHistoriqueSuspensions() {
		obserListHistoriqueSuspensions
				.setAll(DAOFactory.getHistoriqueSuspensionDAO().consulterBDHistoriqueSuspension());
		// envoyer(new obserListHistoriqueSuspension(obserListHistoriqueSuspensions));

	}

	/**
	 * Methode permettant de lire la table des objets reservation_audit.
	 */
	public synchronized static void lireobserListeReservationAudits() {
		obserListReservationAudits.setAll(DAOFactory.getReservationAuditDAO().lireTable());
		// envoyer(new obserListReservationAudit(obserListReservationAudits));
	}

	/**
	 * Methode permettant de lire la liste des objets connection_employe_audit.
	 */
	public synchronized static void lireobserListeConnectionEmployeAudits() {
		obserListConnectionEmployeAudits.setAll(DAOFactory.getConnectionEmployeAuditDAO().lireTable());
		// envoyer(new
		// obserListConnectionEmployeAudit(obserListConnectionEmployeAudits));
	}

	/**
	 * Methode permettant de lire des chambres prises.
	 */
	public synchronized static void lireobserListeChambrePrises() {
		obserListChambrePrises.setAll(DAOFactory.getChambreDAO().demandeChambrePrise());
		// envoyer(new obserListChambre(obserListChambrePrises));
	}

	/**
	 * Methode permettant de lire la liste des clients.
	 */
	public synchronized static void lireobserListeDesClients() {
		obserListClientHotels.setAll(DAOFactory.getClientDAO().consulterBDClient());
		// envoyer(new obserListClientHotel(obserListClientHotels));
	}

	/**
	 * Methode permettant de lire la liste des employes.
	 */
	public synchronized static void lireobserListeDesEmployes() {
		obserListEmployes.setAll(DAOFactory.getEmployeDAO().consulterBDEmployes());
		// envoyer(new obserListEmploye(obserListEmployes));
	}

	/**
	 * Methode permettant de lire la liste des reservations.
	 */
	public synchronized static void lireobserListeDesReservations() {
		obserListReservations.setAll(DAOFactory.getReservationDAO().consulterBDReservation());
		lireobserListeReservationAudits();
		// envoyer(new obserListReservation(obserListReservations));
	}

	public synchronized static ObservableList<Employe> getObserListEmployes() {
		return obserListEmployes;
	}

	public synchronized void setObserListEmployes(ObservableList<Employe> obserListEmployes) {
		DonneesServeur.obserListEmployes = obserListEmployes;
	}

	public synchronized static ObservableList<ClientHotel> getObserListClientHotels() {
		return obserListClientHotels;
	}

	public synchronized void setObserListClientHotels(ObservableList<ClientHotel> obserListClientHotels) {
		DonneesServeur.obserListClientHotels = obserListClientHotels;
	}

	public synchronized ObservableList<Reservation> getObserListReservations() {
		return obserListReservations;
	}

	public synchronized void setObserListReservations(ObservableList<Reservation> obserListReservations) {
		DonneesServeur.obserListReservations = obserListReservations;
	}

	public ObservableList<Chambre> getObserListChambrePrises() {
		return obserListChambrePrises;
	}

	public synchronized void setObserListChambrePrises(ObservableList<Chambre> obserListChambrePrises) {
		DonneesServeur.obserListChambrePrises = obserListChambrePrises;
	}

	public synchronized ObservableList<ConnectionEmployeAudit> getObserListConnectionEmployeAudits() {
		return obserListConnectionEmployeAudits;
	}

	public synchronized void setObserListConnectionEmployeAudits(
			ObservableList<ConnectionEmployeAudit> obserListConnectionEmployeAudits) {
		DonneesServeur.obserListConnectionEmployeAudits = obserListConnectionEmployeAudits;
	}

	public synchronized ObservableList<ReservationAudit> getObserListReservationAudits() {
		return obserListReservationAudits;
	}

	public synchronized void setObserListReservationAudits(
			ObservableList<ReservationAudit> obserListReservationAudits) {
		DonneesServeur.obserListReservationAudits = obserListReservationAudits;
	}

	public synchronized ObservableList<HistoriqueSuspension> getObserListHistoriqueSuspensions() {
		return obserListHistoriqueSuspensions;
	}

	public synchronized void setObserListHistoriqueSuspensions(
			ObservableList<HistoriqueSuspension> obserListHistoriqueSuspensions) {
		DonneesServeur.obserListHistoriqueSuspensions = obserListHistoriqueSuspensions;
	}

	public synchronized ObservableList<EmployeSuspendu> getObserListEmployeSuspendus() {
		return obserListEmployeSuspendus;
	}

	public synchronized void setObserListEmployeSuspendus(ObservableList<EmployeSuspendu> obserListEmployeSuspendus) {
		DonneesServeur.obserListEmployeSuspendus = obserListEmployeSuspendus;
	}

	public static void main(String[] args) {
		DonneesServeur donneesServeur = new DonneesServeur();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(donneesServeur.getObserListChambrePrises().toString());

	}

	/**
	 * Methode permettant d'appeler la procedure vider_chaque_15_secondes permettant de vider
	 * la table des employes suspendus 15 secondes apres qu'il soit insere.
	 */
	public static void executeExecutor() {
		int corePoolSize = Runtime.getRuntime().availableProcessors();
		ScheduledExecutorService execute = Executors.newScheduledThreadPool(corePoolSize);
		execute.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				try {
					Statement statement = ConnectionModele.getInstance().createStatement();
					statement.executeQuery("call vider_chaque_15_secondes");
				} catch (SQLException e) {
					e.printStackTrace();
					MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
				}

			}
		}, 1, 1, TimeUnit.MILLISECONDS);

	}
}
