package Modele.Donnees;

/**
 * Classe permettant d'avoir acces a des donnees statiques.
 * @author akimsoule
 *
 */
public class Statique {
	
	/**
	 * Le sel permmettant de hacher le mot de passe.
	 */
	public static String SALT = "xAKqWeJo2fIDmfYlrIwR7";
	
	/**
	 * Plus petite chambre.
	 */
	public static int PLUS_PETITE_CHAMBRE = 100;
	
	/**
	 * Plus grande chambre.
	 */
	public static int PLUS_GRANDE_CHAMBRE = 599;

}
