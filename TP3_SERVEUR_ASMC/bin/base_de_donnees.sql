
SET SQL_MODE='ALLOW_INVALID_DATES';
drop database if exists gestion_hotel;
create database gestion_hotel;
use gestion_hotel;

drop table if exists employe;
CREATE TABLE employe (
    id_employe INT AUTO_INCREMENT,
    login_employe VARCHAR(40) NOT NULL,
    password_employe VARCHAR(255),
    PRIMARY KEY (id_employe)
)  ENGINE=INNODB;

drop table if exists connection_tentative;
create table connection_tentative(
	id_connection_tentative int auto_increment,
    login_connection_tentative varchar(40) not null,
    nombre_connection_tentative int NOT NULL,
     PRIMARY KEY (id_connection_tentative)
) ENGINE=INNODB;

drop table if exists employe_suspendu;
create TABLE employe_suspendu(
	id_employe_suspendu INT AUTO_INCREMENT,
    login_employe_suspendu VARCHAR(40) NOT NULL,
    date_de_suspension TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id_employe_suspendu)
) ENGINE=INNODB;

drop table if exists employe_suspendu_audit;
create TABLE employe_suspendu_audit(
	id_employe_suspendu_audit INT AUTO_INCREMENT,
    login_employe_suspendu_audit VARCHAR(40) NOT NULL,
    varchar_ varchar(40) NOT null,
    date_de_declenchement TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id_employe_suspendu_audit)
) ENGINE=INNODB;


drop table if exists client;
CREATE TABLE client (
    id_client INT AUTO_INCREMENT,
    nom_client VARCHAR(40) NOT NULL,
    prenom_client VARCHAR(40) NOT NULL,
    PRIMARY KEY (id_client)
)  ENGINE=INNODB;

drop table if exists reservation;
CREATE TABLE reservation (
    id_reservation INT AUTO_INCREMENT,
    date_de_debut_sejour TIMESTAMP,
    date_de_fin_sejour TIMESTAMP,
    nombre_enfant INT NOT NULL,
    nombre_adulte INT NOT NULL,
    id_employe INT,
    id_client INT,
    chambre INT,
    PRIMARY KEY (id_reservation),
    FOREIGN KEY (id_client)
        REFERENCES client (id_client),
    FOREIGN KEY (id_employe)
        REFERENCES employe (id_employe)
)  ENGINE=INNODB;

drop table if exists connection_employe_audit;
CREATE TABLE connection_employe_audit (
    login_employe VARCHAR(40) NOT NULL,
    connection_audit VARCHAR(40),
    date_de_connection TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)  ENGINE=INNODB;

drop table if exists reservation_audit;
CREATE TABLE reservation_audit (
    acteur VARCHAR(40) NOT NULL,
    type_update VARCHAR(40) NOT NULL,
    debut_audit TIMESTAMP,
    fin_audit TIMESTAMP,
    nombre_enfant_audit INT NOT NULL,
    nombre_adulte_audit INT NOT NULL,
    nom_prenom_client_audit varchar(40),
    chambre_audit INT,
    date_de_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)  ENGINE=INNODB;

DROP TRIGGER IF EXISTS reservation_after_insert;
DELIMITER //
CREATE  TRIGGER reservation_after_insert
AFTER INSERT
   ON reservation FOR EACH ROW
BEGIN
DECLARE nom_prenom_client_var_audit varchar(40);
DECLARE login varchar (40);
DECLARE varchar_reservation varchar(40);
select login_employe into login from employe where id_employe = New.id_employe;
select concat(nom_client, ', ', prenom_client) into nom_prenom_client_var_audit from client where id_client = New.id_client;
select 'CREATION' into varchar_reservation;
   -- Insert record into audit table
   INSERT INTO reservation_audit
   (acteur , type_update , debut_audit , fin_audit,
    nombre_enfant_audit,
    nombre_adulte_audit,
    nom_prenom_client_audit,
    chambre_audit)
   VALUES
   (login,  varchar_reservation, NEW.date_de_debut_sejour, NEW.date_de_fin_sejour,
   NEW.nombre_enfant, NEW.nombre_adulte, nom_prenom_client_var_audit, NEW.chambre);
END; //
DELIMITER ;

DROP TRIGGER IF EXISTS employe_suspendu_after_insert;
DELIMITER //
CREATE TRIGGER employe_suspendu_after_insert
AFTER INSERT
   ON employe_suspendu FOR EACH ROW
BEGIN
DECLARE var_varchar_ varchar(40);
select 'SUSPENDU' into var_varchar_;
   -- Insert record into audit table
   INSERT INTO employe_suspendu_audit
   (login_employe_suspendu_audit,
   varchar_)
   VALUES
   (NEW.login_employe_suspendu, var_varchar_);
END; //
DELIMITER ;

DROP TRIGGER IF EXISTS employe_suspendu_before_delete;
DELIMITER //
CREATE TRIGGER employe_suspendu_before_delete
BEFORE DELETE
   ON employe_suspendu FOR EACH ROW
BEGIN
DECLARE var_varchar_ varchar(40);
select 'SUSPENSION_LEVE' into var_varchar_;
   -- Insert record into audit table
   INSERT INTO employe_suspendu_audit
   (login_employe_suspendu_audit,
   varchar_)
   VALUES
   (old.login_employe_suspendu, var_varchar_);
END; //
DELIMITER;

DROP procedure IF EXISTS vider_chaque_15_secondes;
DELIMITER  |
CREATE PROCEDURE vider_chaque_15_secondes ()
BEGIN
	DECLARE v_finish INTEGER DEFAULT 0;
	DECLARE var_login varchar(40);
    DECLARE var_date timestamp;
    DECLARE var_date_plus_15 timestamp;
    DECLARE curs CURSOR FOR SELECT login_employe_suspendu, date_de_suspension from employe_suspendu;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finish = 1;
   
    open curs;
    read_loop: LOOP
		FETCH curs into var_login, var_date;
        IF v_finish = 1 then 
			leave read_loop;
		end if;
        select DATE_ADD(var_date, INTERVAL 15 SECOND) into var_date_plus_15;
        
        if (now()  > var_date_plus_15) THEN
			delete from employe_suspendu where login_employe_suspendu = var_login;
            delete from connection_tentative where login_connection_tentative = var_login;
		end if;
        end loop;
	close curs;
 
END |
DELIMITER ;

SHOW PROCESSLIST; 

drop event if exists activer_employe;
SET GLOBAL event_scheduler = ON;
CREATE EVENT if not exists activer_employe
ON SCHEDULE EVERY 1 second
STARTS CURRENT_TIMESTAMP
ON COMPLETION NOT PRESERVE ENABLE
DO
   call vider_chaque_15_secondes();


insert into employe (login_employe, password_employe) values ('admin', 'kg/qUfIj0sal2TyB9FzzUoRbBAjp+6YhfIq9beU/rq30Lr6AsKfrgbEGPmBLFVirWOsjJwUT5UKczmUMN5xo3Q==');
insert into employe (login_employe, password_employe) values ('soule', 'Q0b8UVLtUp2SEk6pQebPrIdHMBInc6aEh4AdA5lEKPo0UlitwqVFfB1Oysv1p/rvABvyPr0nMy3dyix83NH/gw==');







