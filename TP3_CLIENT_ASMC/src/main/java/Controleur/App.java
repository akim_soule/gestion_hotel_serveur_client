package Controleur;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class App extends Application {

	private AnchorPane root;

	public void start(Stage primaryStage) throws Exception {
		root = FXMLLoader.load(getClass().getResource("../loginFXML.fxml"));
		Scene scene = new Scene(root);

		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Login");
		primaryStage.show();
	}
	public static void main(String[] args) {
		Application.launch(args);
		
	}

}
