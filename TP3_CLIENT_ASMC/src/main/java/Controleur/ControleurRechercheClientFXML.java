package Controleur;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import Modele.package_utilitaire.UtilitaireGestionFormulaire;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ControleurRechercheClientFXML implements Initializable {

	public static ObservableList<String> observableListClient = FXCollections.observableArrayList();
	StringProperty messageProperty;

	@FXML
	private ListView<String> listViewClientId;

	@FXML
	private RadioButton buttonRadioNomId;

	@FXML
	private RadioButton buttonRadioPrenomId;

	@FXML
	private TextField textFieldRechercheId;

	@FXML
	private Button buttonSelectionnerId;

	@FXML
	private Text messageTextId;

	private Stage stage;

	@FXML
	private void buttonSelectionnerOnAction(ActionEvent event) {
		ControleurVueGestion.nomPrenomProperty2.setValue(listViewClientId.getSelectionModel().getSelectedItem().toString());
		
		Stage stage = (Stage) buttonSelectionnerId.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void textFieldRechercheOnAction(ActionEvent event) {

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		messageProperty = new SimpleStringProperty();
		messageTextId.textProperty().bind(messageProperty);
		listViewClientId.getItems().clear();
		listViewClientId.setItems(observableListClient);
		buttonSelectionnerId.setDisable(true);

		textFieldRechercheId.textProperty().addListener(new ChangeListener<String>() {

			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					String newValueTranforme = UtilitaireGestionFormulaire.transformNomPrenom(newValue);
					List <String> listRecherche = null;
					if (buttonRadioPrenomId.isSelected()) {
						listRecherche = observableListClient.stream().filter(x -> x.split(",")[1].trim().startsWith(newValueTranforme)).collect(Collectors.toList());
					}else {
						listRecherche = observableListClient.stream().filter(x -> x.split(",")[0].trim().startsWith(newValueTranforme)).collect(Collectors.toList());
					}
					
					listViewClientId.setItems(FXCollections.observableArrayList(listRecherche));
				}
				
			}
		});

		listViewClientId.setOnMouseClicked(new EventHandler<Event>() {

			public void handle(Event event) {
				buttonSelectionnerId.setDisable(false);
				
			}
		});

	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
