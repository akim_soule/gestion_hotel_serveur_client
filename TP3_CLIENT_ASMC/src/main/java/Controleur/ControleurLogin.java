package Controleur;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

import Modele.Client;
import Modele.ExceptionPersonnalisee;
import Modele.package_enumeration.SIGNAL;
import Modele.package_logger.MyLoggingMessageErreur;
import Modele.package_utilitaire.UtilitaireGestionFormulaire;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ControleurLogin implements Initializable {

	@FXML
	private PasswordField textFieldPasswordId;

	@FXML
	private Text messageId;

	@FXML
	private TextField textFieldLoginId;

	@FXML
	private ProgressIndicator serviceRunningIndicator;

	@FXML
	private Button buttonConnectId;

	public static StringProperty messageProperty;
	private String loginFXML;
	private Client client;
	private Service<Void> service;

	private Stage stage;

	@FXML
	public void buttonConnectOnAction(ActionEvent event) {
		boolean validation = true;
		String message = "";
		if (textFieldLoginId.getText().isEmpty()) {
			validation = false;
			message += "Le champ login est vide\n";
		} else {
			if (!UtilitaireGestionFormulaire.caractereValide(textFieldLoginId.getText())) {
				validation = false;
				message += "Le champ login doit contenir des caracteres valides\n";
			}
		}
		if (textFieldPasswordId.getText().isEmpty()) {
			validation = false;
			message += "Le champ password est vide\n";
		}
		if (!validation) {
			new ExceptionPersonnalisee("Erreur", message);
		} else {
			service.restart();
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		messageProperty = new SimpleStringProperty();
		serviceRunningIndicator.setVisible(false);
		messageId.textProperty().bind(messageProperty);
		messageProperty.addListener(new ChangeListener<String>() {

			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					serviceRunningIndicator.setVisible(false);
					if (newValue.equals("CONNECTED")) {
						loginFXML = textFieldLoginId.getText();
						Stage stage = (Stage) buttonConnectId.getScene().getWindow();
						stage.close();
						openFxmlApproprie("../VueGestion.fxml");
					} else if (newValue.equals("ADMIN_CONNECTED")) {
						loginFXML = textFieldLoginId.getText();
						Stage stage = (Stage) buttonConnectId.getScene().getWindow();
						stage.close();
						openFxmlApproprie("../VueAdmin.fxml");
					} else {
						buttonConnectId.setDisable(false);
					}

				}
			}
		});

		service = new Service<Void>() {

			@Override
			protected void succeeded() {
				serviceRunningIndicator.setVisible(false);
				buttonConnectId.setDisable(false);
				super.succeeded();
			}

			@Override
			protected Task<Void> createTask() {
				// TODO Auto-generated method stub
				return new Task<Void>() {
					protected Void call() throws Exception {
						client = null;
						serviceRunningIndicator.setVisible(true);
						buttonConnectId.setDisable(true);
						String login = textFieldLoginId.getText();
						String passwordHache = UtilitaireGestionFormulaire
								.hashPassword(textFieldPasswordId.getText(), "xAKqWeJo2fIDmfYlrIwR7").get();
						client = new Client("127.0.0.1", 2345);
						client.envoyer(SIGNAL.LOGIN + "-" + login + "-" + passwordHache);
						return null;
					}
				};
			}
		};
		


	}

	private void openFxmlApproprie(String string) {
		FXMLLoader loaderAjouter = new FXMLLoader(this.getClass().getResource(string));
		Stage stage = new Stage();
		AnchorPane r;
		try {
			r = (AnchorPane) loaderAjouter.load();

			Scene scene = new Scene(r);
			stage.setScene(scene);
			stage.setResizable(false);
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		if (string.equals("../VueGestion.fxml")) {
			ControleurVueGestion ajoutController = loaderAjouter.<ControleurVueGestion>getController();
			ajoutController.setStage(stage);
			ajoutController.setData(UtilitaireGestionFormulaire.transformNomPrenom(loginFXML), client);
		}
		if (string.equals("../VueAdmin.fxml")) {

			ControleurAudit ajoutController = loaderAjouter.<ControleurAudit>getController();
			ajoutController.setStage(stage);
			ajoutController.setData(UtilitaireGestionFormulaire.transformNomPrenom(loginFXML), client);
		}
		stage.show();
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage pStage) {
		this.stage = pStage;
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				deconnection();
			}
		});
	}

	private void deconnection() {
		if (client != null) {
			client.envoyer(SIGNAL.VUEGESTION_DECONNEXION);
			client.closerConnection();
		}
		client = null;
		Platform.exit();

	}

}
