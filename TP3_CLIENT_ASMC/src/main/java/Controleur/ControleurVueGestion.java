package Controleur;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.stream.Collectors;

import Modele.Client;
import Modele.ExceptionPersonnalisee;
import Modele.objet.classe_java.Reservation;
import Modele.package_enumeration.SIGNAL;
import Modele.package_logger.MyLoggingMessageErreur;
import Modele.package_utilitaire.UtilitaireGestionFormulaire;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ControleurVueGestion implements Initializable {

	public static ObservableList<String> observableListClient = FXCollections.observableArrayList();
	public static ObservableList<Reservation> observableListReservation = FXCollections.observableArrayList();
	public static StringProperty messageProperty2 = new SimpleStringProperty();
	public static StringProperty nomPrenomProperty2 = new SimpleStringProperty();
	public static IntegerProperty chambreProperty2 = new SimpleIntegerProperty();

	public Stage stage;

	private String userNom;
	private Client client;

	@FXML
	private TextField textFieldNomClient;

	@FXML
	private TextField textFieldPrenomClient;

	@FXML
	private Button buttonModifierClientId;

	@FXML
	private Button buttonCreerCLientId;

	@FXML
	private ListView<String> listViewClientId;

	@FXML
	private Button buttonSupprimerClientId;

	@FXML
	private RadioButton buttonRadioNomId;

	@FXML
	private ToggleGroup nomprenom;

	@FXML
	private RadioButton buttonRadioPrenomId;

	@FXML
	private TextField textFieldRechercheClient;

	@FXML
	private Button buttonCreerId;

	@FXML
	private Button buttonSupprimerReservationId;

	@FXML
	private DatePicker datePicketDateDeDebutSejour;

	@FXML
	private DatePicker datePickerDateDeFinSejour;

	@FXML
	private Button buttonRechercherClientId;

	@FXML
	private TableView<Reservation> tableViewReservation;

	@FXML
	private TableColumn<Reservation, Integer> colonneId;

	@FXML
	private TableColumn<Reservation, String> colonneNom;

	@FXML
	private TableColumn<Reservation, String> colonnePrenom;

	@FXML
	private TableColumn<Reservation, String> colonneDateDeDebutSejour;

	@FXML
	private TableColumn<Reservation, String> colonneDateDeFinSejour;

	@FXML
	private TableColumn<Reservation, Integer> colonneChambre;

	@FXML
	private TableColumn<Reservation, Integer> colonneNbreAdulte;

	@FXML
	private TableColumn<Reservation, Integer> colonneNbreEnfants;

	@FXML
	private Text nomPrenomClientId;

	@FXML
	private Button consulterClientId;

	@FXML
	private TextField textFieldChambre;

	@FXML
	private TextField textFieldAdulte;

	@FXML
	private TextField textFieldEnfant;

	@FXML
	private Button buttonConsulterId;

	@FXML
	private ChoiceBox<String> heureDarriveeId;

	@FXML
	private ChoiceBox<String> minuteDarriveeId;

	@FXML
	private ChoiceBox<String> minuteDeDepartId;

	@FXML
	private ChoiceBox<String> heureDeDepartId;

	@FXML
	private Button buttonModifierId;

	@FXML
	private Text messageTextId;

	@FXML
	private Button buttonDeconnexionId;

	@FXML
	private Text employeConnecteId;

	@FXML
	private ProgressIndicator serviceRunningIndicator;

	@FXML
	private ContextMenu contextMenuId;

	@FXML
	private MenuItem contextMenuAjouterId;

	@FXML
	public void buttonCreerClientOnAction(ActionEvent event) {
		boolean ok = true;
		String message = "";
		String nomClient = "";
		String prenomClient = "";
		if (textFieldNomClient.getText().isEmpty()) {
			ok = false;
			message += "Le champs nom ne doit pas etre vide\n";
		}else {
			nomClient = UtilitaireGestionFormulaire.transformNomPrenom(textFieldNomClient.getText());
			if (!UtilitaireGestionFormulaire.caractereValide(nomClient)) {
				ok = false;
				message += "Le nom doit avoir des caracteres valides\n";
			}
		}
		
		if (textFieldPrenomClient.getText().isEmpty()) {
			ok = false;
			message += "Le champs prenom ne doit pas etre vide\n";
		}else {
			prenomClient = UtilitaireGestionFormulaire.transformNomPrenom(textFieldPrenomClient.getText());
			if (!UtilitaireGestionFormulaire.caractereValide(prenomClient)) {
				ok = false;
				message += "Le prenom doit avoir des caracteres valides\n";
			}
		}
		
		if (!ok) {
			new ExceptionPersonnalisee("Erreur", message);
		} else {
			client.envoyer(SIGNAL.VUEGESTION_CREER_CLIENT + "-" + nomClient + "-" + prenomClient);
			textFieldNomClient.setText("");
			textFieldPrenomClient.setText("");
		}
		
	}

	@FXML
	private void buttonCreerReservationOnAction(ActionEvent event) {
		if (validationForumulaireReservation()) {
			creerReservation();
		}
	}

	private boolean validationForumulaireReservation() {
		boolean validation = true;
		String message = "";
		if (nomPrenomClientId.getText() == null) {
			validation = false;
			message += "- Vous n'avez pas selectionne le nom et prenom du client\n";
		}
		if (datePicketDateDeDebutSejour.getValue() == null) {
			validation = false;
			message += "- Vous n'avez pas selectionne la date de debut de sejour\n";
		}
		if (heureDarriveeId.getValue() == null) {
			validation = false;
			message += "- Vous n'avez pas selectionne l'heure d'arrivee\n";
		}
		if (minuteDarriveeId.getValue() == null) {
			validation = false;
			message += "- Vous n'avez pas selectionne la minute d'arrivee\n";
		}
		if (datePickerDateDeFinSejour.getValue() == null) {
			validation = false;
			message += "- Vous n'avez pas selectionne la date de fin de sejour\n";
		}
		if (heureDeDepartId.getValue() == null) {
			validation = false;
			message += "- Vous n'avez pas selectionne l'heure de depart\n";
		}
		if (minuteDeDepartId.getValue() == null) {
			validation = false;
			message += "- Vous n'avez pas selectionne la minute de depart\n";
		}
		if (textFieldChambre.getText().isEmpty()) {
			validation = false;
			message += "- Vous n'avez pas entre le numero de chambre\n";
		}else {
			try {
				int nombre = Integer.parseInt(textFieldChambre.getText());
				if (!(nombre >= 100 && nombre <= 500)){
					validation = false;
					message += "- Le numero de la chambre doit etre compris entre 100 et 500\n";
				}
			}catch(Exception e) {
				validation = false;
				message += "- Le numero de la chambre doit etre un entier\n";
				
			}
		}
		if (textFieldAdulte.getText().isEmpty()) {
			validation = false;
			message += "- Vous n'avez pas entre le nombre d'adulte\n";
		}
		if (textFieldEnfant.getText().isEmpty()) {
			validation = false;
			message += "- Vous n'avez pas entre le nombre d'enfants\n";
		}

		if (validation == false) {
			new ExceptionPersonnalisee("Erreur reservation", message);
		}
		return validation;

	}

	private void creerReservation() {
		int idClient = Integer.parseInt(nomPrenomClientId.getText().split(",")[0]);
		String nomClient = nomPrenomClientId.getText().split(",")[1].trim();
		String prenomClient = nomPrenomClientId.getText().split(",")[2].trim();
		LocalDate dateDeDebutSejour = datePicketDateDeDebutSejour.getValue();
		String heureDarrivee = heureDarriveeId.getValue();
		String minuteDarrivee = minuteDarriveeId.getValue();
		String dateDebut = dateDeDebutSejour.toString().replace("-", "") + heureDarrivee + minuteDarrivee + "00";

		LocalDate dateDeFinSejour = datePickerDateDeFinSejour.getValue();
		String heureDeDepart = heureDeDepartId.getValue();
		String minuteDeDepart = minuteDeDepartId.getValue();
		String dateFin = dateDeFinSejour.toString().replace("-", "") + heureDeDepart + minuteDeDepart + "00";

		System.out.println(dateDebut);
		System.out.println(dateFin);
		int numeroChambre = Integer.parseInt(textFieldChambre.getText());
		int nombreAdulte = Integer.parseInt(textFieldAdulte.getText());
		int nombreEnfant = Integer.parseInt(textFieldEnfant.getText());
		
		System.out.println(SIGNAL.VUEGESTION_CREER_RESERVATION + "/" + userNom + "/" + idClient + "/" + nomClient + "/"
				+ prenomClient + "/" + dateDebut + "/" + dateFin + "/" + numeroChambre + "/" + nombreAdulte + "/"
				+ nombreEnfant);

		client.envoyer(SIGNAL.VUEGESTION_CREER_RESERVATION + "/" + userNom + "/" + idClient + "/" + nomClient + "/"
				+ prenomClient + "/" + dateDebut + "/" + dateFin + "/" + numeroChambre + "/" + nombreAdulte + "/"
				+ nombreEnfant);
		viderLesChampsreservation();

	}

	private void viderLesChampsreservation() {
		nomPrenomProperty2.setValue("");
		datePicketDateDeDebutSejour.setValue(null);
		datePickerDateDeFinSejour.setValue(null);
		heureDarriveeId.setValue(null);
		minuteDarriveeId.setValue(null);
		heureDeDepartId.setValue(null);
		minuteDeDepartId.setValue(null);
		chambreProperty2.setValue(null);
		textFieldChambre.setText("");
		textFieldAdulte.setText("");
		textFieldEnfant.setText("");

	}

	private void rafraichirTableView() {
		tableViewReservation.getItems().clear();
		client.envoyer(SIGNAL.VUEGESTION_DEMANDE_RESERVATION);
	}

	@FXML
	private void buttonDeconnexionOnAction(ActionEvent event) {
		this.stage.close();
		//openFxmlApproprie("../loginFXML.fxml");
	}

	private void openFxmlApproprie(String string) {
		FXMLLoader loaderAjouter = new FXMLLoader(this.getClass().getResource(string));
		Stage stage = new Stage();
		AnchorPane r;
		try {
			r = (AnchorPane) loaderAjouter.load();

			Scene scene = new Scene(r);
			stage.setScene(scene);
			stage.setResizable(false);
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		if (string.equals("../loginFXML.fxml")) {
			ControleurLogin ajoutController = loaderAjouter.<ControleurLogin>getController();
			ajoutController.setStage(stage);
		}

		stage.show();

	}

	@FXML
	private void buttonModifierClientOnAction(ActionEvent event) {
		String nomClient = UtilitaireGestionFormulaire.transformNomPrenom(textFieldNomClient.getText());
		String prenomClient = UtilitaireGestionFormulaire.transformNomPrenom(textFieldPrenomClient.getText());
		String ancienNomPrenom = listViewClientId.getSelectionModel().getSelectedItem();
		if (ancienNomPrenom != null) {
			String ancienNom = ancienNomPrenom.split(",")[0].trim();
			String ancienPrenom = ancienNomPrenom.split(",")[1].trim();
			if (nomClient.equals(ancienNom) && prenomClient.equals(ancienPrenom)) {
				messageProperty2.setValue("Les noms et prenoms sont les memes");
			} else {
				client.envoyer(SIGNAL.VUEGESTION_MODIFIER_CLIENT + "-" + nomClient + "/" + ancienNom + "-"
						+ prenomClient + "/" + ancienPrenom);
				listViewClientId.getItems().set(listViewClientId.getSelectionModel().getSelectedIndex(),
						nomClient + ", " + prenomClient);
			}

		} else {
			messageProperty2.setValue("Veuiller selectionner un client et permettre sa modification");
		}
	}

	@FXML
	private void buttonRecherClientOnAction(ActionEvent event) {
		FXMLLoader loaderAjouter = new FXMLLoader(this.getClass().getResource("../RechercheClient.fxml"));
		Stage stage = new Stage();
		AnchorPane r;
		try {
			r = (AnchorPane) loaderAjouter.load();

			Scene scene = new Scene(r);
			stage.setScene(scene);
			stage.setResizable(false);

		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}

		ControleurRechercheClientFXML ajoutController = loaderAjouter.<ControleurRechercheClientFXML>getController();
		ajoutController.setStage(stage);

		stage.show();
	}

	@FXML
	private void buttonSupprimerClientOnAction(ActionEvent event) {
		String clique = listViewClientId.getSelectionModel().getSelectedItem();
		if (clique != null) {
			String nom = clique.split(",")[0].trim();
			String prenom = clique.split(",")[1].trim();
			client.envoyer(SIGNAL.VUEGESTION_SUPPRIME_CLIENT + "-" + nom + "-" + prenom);
			// UtilitaireClient.envoyer(SIGNAL.VUEGESTION_SUPPRIME_CLIENT + "-" + nom + "-"
			// + prenom);
			textFieldNomClient.setText("");
			textFieldPrenomClient.setText("");
		}else {
			new ExceptionPersonnalisee("Erreur", "Vous n'avez selectionne aucun client");
		}
		
	}

	@FXML
	private void buttonSupprimerReservationOnAction(ActionEvent event) {
		Reservation reservation = tableViewReservation.getSelectionModel().getSelectedItem();
		if (reservation != null) {
			client.envoyer(
					SIGNAL.VUEGESTION_SUPPRIME_RESERVATION + "/" + reservation.getId() + "/" + reservation.getChambre()
							+ "/" + reservation.getDateDebutSejour() + "/" + reservation.getDateFinSejour()+
							"/"+userNom+"/"+reservation.getNombre_adulte()+"/"+reservation.getNombre_enfant());

			viderLesChampsreservation();

		} else {
			new ExceptionPersonnalisee("Erreur", "Vous n'avez selectionné une réservation");
		}

	}

	@FXML
	private void consulterClientOnAction(ActionEvent event) {
		client.envoyer(SIGNAL.VUEGESTION_DEMANDE_CLIENT);
	}

	@FXML
	private void bouttonModifierReservationOnAction(ActionEvent event) {
		if (validationForumulaireReservation()) {
			modifierReservation();
		}
	}

	private void modifierReservation() {
		Reservation reservation = tableViewReservation.getSelectionModel().getSelectedItem();
		if (reservation != null) {
			LocalDate dateDeDebutSejour = datePicketDateDeDebutSejour.getValue();
			String heureDarrivee = heureDarriveeId.getValue();
			String minuteDarrivee = minuteDarriveeId.getValue();
			String dateDebut = dateDeDebutSejour.toString().replace("-", "") + heureDarrivee + minuteDarrivee + "00";

			LocalDate dateDeFinSejour = datePickerDateDeFinSejour.getValue();
			String heureDeDepart = heureDeDepartId.getValue();
			String minuteDeDepart = minuteDeDepartId.getValue();
			String dateFin = dateDeFinSejour.toString().replace("-", "") + heureDeDepart + minuteDeDepart + "00";

			int numeroChambre = Integer.parseInt(textFieldChambre.getText());
			int nombreAdulte = Integer.parseInt(textFieldAdulte.getText());
			int nombreEnfant = Integer.parseInt(textFieldEnfant.getText());
			client.envoyer("VUEGESTION_MODIFIER_RESERVATION" + "/" + reservation.getId() + "/" + dateDebut + "/"
					+ dateFin + "/" + numeroChambre + "/" + nombreAdulte + "/" + nombreEnfant+ "/" + userNom+ "/" + 
					reservation.getNom()+", "+reservation.getPrenom());
			
			viderLesChampsreservation();
		} else {
			new ExceptionPersonnalisee("Erreur", "Vous n'avez selectionné aucune reservation");
		}

	}

	@FXML
	private void consulterOnAction(ActionEvent event) {
		rafraichirTableView();
	}

	@FXML
	private void contextMenuAjouterOnAction(ActionEvent event) {
		System.out.println("clique sur ajouter");
	}

	@FXML
	private void contextMenuSupprimerOnAction(ActionEvent event) {
		System.out.println("clique sur suppriner");
	}

	@FXML
	private void onContextMenuRequestedAction(ContextMenuEvent event) {
		System.out.println("clique sur conexte menu");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initiliaserLesVariables();
		initializeElementsFX();

		listenerTableViewListView();

	}

	private void initiliaserLesVariables() {
		nomPrenomClientId.textProperty().bind(nomPrenomProperty2);
		messageTextId.textProperty().bind(messageProperty2);
		listViewClientId.setItems(observableListClient);
		buttonCreerCLientId.setDisable(true);
		buttonModifierClientId.setDisable(true);

	}

	private void listenerTableViewListView() {

		textFieldNomClient.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.equals("")) {
					buttonCreerCLientId.setDisable(true);
				} else {
					buttonCreerCLientId.setDisable(false);
				}
			}
		});

		listViewClientId.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					buttonModifierClientId.setDisable(false);
					String nom = newValue.split(",")[0].trim();
					String prenom = newValue.split(",")[1].trim();
					textFieldNomClient.setText(nom);
					textFieldPrenomClient.setText(prenom);
				}
			}
		});

		tableViewReservation.getSelectionModel().selectedItemProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue != null) {
				Reservation reservation = (Reservation) newValue;
				LocalDate localDateArrivee = LocalDate.parse(reservation.getDateDebutSejour().substring(0, 10));
				String heureDebut = reservation.getDateDebutSejour().substring(11, 13);
				String minuteDebbut = reservation.getDateDebutSejour().substring(14, 16);
				datePicketDateDeDebutSejour.setValue(localDateArrivee);
				heureDarriveeId.setValue(heureDebut);
				minuteDarriveeId.setValue(minuteDebbut);

				LocalDate localDateDepart = LocalDate.parse(reservation.getDateFinSejour().substring(0, 10));
				String heureDepart = reservation.getDateFinSejour().substring(11, 13);
				String minuteDepart = reservation.getDateFinSejour().substring(14, 16);
				datePickerDateDeFinSejour.setValue(localDateDepart);
				heureDeDepartId.setValue(heureDepart);
				minuteDeDepartId.setValue(minuteDepart);

				textFieldChambre.setText(reservation.getChambre() + "");
				textFieldAdulte.setText(reservation.getNombre_adulte() + "");
				textFieldEnfant.setText(reservation.getNombre_enfant() + "");

				nomPrenomProperty2.setValue(
						reservation.getId_client() + ", " + reservation.getNom() + ", " + reservation.getPrenom());

			}
		});

		textFieldRechercheClient.textProperty().addListener(new ChangeListener<String>() {

			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					String newValueTranforme = UtilitaireGestionFormulaire.transformNomPrenom(newValue);
					List<String> listRecherche = null;
					if (buttonRadioPrenomId.isSelected()) {
						listRecherche = observableListClient.stream()
								.filter(x -> x.split(",")[1].trim().startsWith(newValueTranforme))
								.collect(Collectors.toList());
					} else {
						listRecherche = observableListClient.stream()
								.filter(x -> x.split(",")[0].trim().startsWith(newValueTranforme))
								.collect(Collectors.toList());
					}

					listViewClientId.setItems(FXCollections.observableArrayList(listRecherche));
				}

			}
		});

	}

	private void initializeElementsFX() {
		serviceRunningIndicator.setVisible(false);
		ObservableList<String> obserHeure = FXCollections.observableArrayList();

		for (int i = 0; i <= 23; i++) {
			if (i < 10) {
				obserHeure.add(String.valueOf("0" + i));
			} else {
				obserHeure.add(String.valueOf(i));
			}
		}
		heureDeDepartId.setItems(obserHeure);
		heureDarriveeId.setItems(obserHeure);

		ObservableList<String> obserMinute = FXCollections.observableArrayList(Arrays.asList("00", "15", "30", "45"));

		minuteDarriveeId.setItems(obserMinute);
		minuteDeDepartId.setItems(obserMinute);

		tableViewReservation.setItems(observableListReservation);
		colonneId.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("id"));
		colonneNom.setCellValueFactory(new PropertyValueFactory<Reservation, String>("nom"));
		colonnePrenom.setCellValueFactory(new PropertyValueFactory<Reservation, String>("prenom"));
		colonneDateDeDebutSejour.setCellValueFactory(new PropertyValueFactory<Reservation, String>("dateDebutSejour"));
		colonneDateDeFinSejour.setCellValueFactory(new PropertyValueFactory<Reservation, String>("dateFinSejour"));
		colonneChambre.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("chambre"));
		colonneNbreAdulte.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("nombre_adulte"));
		colonneNbreEnfants.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("nombre_enfant"));

	}

	public void setStage(Stage pStage) {
		this.stage = pStage;
		Platform.runLater(()->{
			stage.setOnHiding(event -> {
				
				//if (client.getConnected().getValue() == true) {
					client.envoyer(SIGNAL.VUEGESTION_DECONNEXION);
					client.closerConnection();
				//}
				
				this.stage.close();
				openFxmlApproprie("../loginFXML.fxml");
			});
		});
		
	}

	public void setData(String login, Client pClient) {
		this.userNom = login;
		this.client = pClient;
		client.envoyer(SIGNAL.VUEGESTION_DEMANDE_RESERVATION);
		client.envoyer(SIGNAL.VUEGESTION_DEMANDE_CLIENT);
		employeConnecteId.setText(employeConnecteId.getText() + userNom);
	}

}
