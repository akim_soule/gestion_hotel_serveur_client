package Controleur;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.stream.Collectors;

import Modele.Client;
import Modele.ExceptionPersonnalisee;
import Modele.donnees.Statique;
import Modele.objet.classe_java.ConnectionEmployeAudit;
import Modele.objet.classe_java.Employe;
import Modele.objet.classe_java.EmployeSuspendu;
import Modele.objet.classe_java.HistoriqueSuspension;
import Modele.objet.classe_java.ReservationAudit;
import Modele.package_enumeration.SIGNAL;
import Modele.package_logger.MyLoggingMessageErreur;
import Modele.package_utilitaire.UtilitaireGestionFormulaire;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ControleurAudit implements Initializable {

	public static ObservableList<ConnectionEmployeAudit> observableListConnectionEmployeAudits = FXCollections
			.observableArrayList();
	public static ObservableList<ReservationAudit> observableListReservationAudits = FXCollections
			.observableArrayList();
	public static ObservableList<Employe> observableListEmployes = FXCollections.observableArrayList();
	public static ObservableList<EmployeSuspendu> observableListEmployeSuspendus = FXCollections.observableArrayList();
	public static ObservableList<HistoriqueSuspension> observableListHistoriqueSuspensions = FXCollections
			.observableArrayList();

	@FXML
	private TableView<ConnectionEmployeAudit> tableViewConnectionAudit;

	@FXML
	private TableColumn<ConnectionEmployeAudit, String> colonneLoginId;

	@FXML
	private TableColumn<ConnectionEmployeAudit, String> colonneColonneAuditId;

	@FXML
	private TableColumn<ConnectionEmployeAudit, String> colonneDateDeConnectionId;

	@FXML
	private Button buttonActualiserConnectionAuditId;

	@FXML
	private TableView<ReservationAudit> tableViewReservationAudit;

	@FXML
	private TableColumn<ReservationAudit, String> colonneActeurId;

	@FXML
	private TableColumn<ReservationAudit, String> colonneTypeUpdateId;

	@FXML
	private TableColumn<ReservationAudit, String> colonneDeburAuditId;

	@FXML
	private TableColumn<ReservationAudit, String> colonneFinAuditId;

	@FXML
	private TableColumn<ReservationAudit, Integer> colonneNombreEnfantAuditId;

	@FXML
	private TableColumn<ReservationAudit, String> colonneNomPrenomClientAuditId;

	@FXML
	private TableColumn<ReservationAudit, Integer> colonneChambreAuditId;

	@FXML
	private TableColumn<ReservationAudit, String> colonneDateUpdateId;

	@FXML
	private Button buttonActualiserLoginId;

	@FXML
	private Button buttonSupprimerLoginId;

	@FXML
	private Button buttonActualiserReservationAuditId;

	@FXML
	private ListView<Employe> listViewEmployeId;

	@FXML
	private ListView<EmployeSuspendu> listViewEmployeSuspendusId;

	@FXML
	private ListView<HistoriqueSuspension> listViewHistoriqueSuspensionId;

	@FXML
	private TextField textFieldRechercheId;

	@FXML
	private TextField textFieldLoginId;

	@FXML
	private Button buttonActualiserEmployeLoginId;

	@FXML
	private Button buttonSupprimerEmployeLoginId;

	@FXML
	private PasswordField textFieldPasswordId;

	@FXML
	private Button buttonAjouterId;

	@FXML
	private Button buttonSupprimerEmployeSuspenduId;

	@FXML
	private Button buttonActualiserEmployeSuspenduId;

	@FXML
	private Button buttonActualiserHistoriqueSuspensionId;

	@FXML
	private Button buttonDeconnexionId;

	@FXML
	private Text messageFromServerId;

	public static StringProperty messageProperty;

	private Client client;
	private String login;
	private Stage stage;

	@FXML
	private void actualiserConnectionAuditOnAction(ActionEvent event) {
		client.envoyer(SIGNAL.VUE_CONTROLEUR_AUDIT_DEMANDE_LISTE_CONNECTION_EMPLOYE);
	}

	@FXML
	private void actualiserReservationAuditOnAction(ActionEvent event) {
		client.envoyer(SIGNAL.VUE_CONTROLEUR_AUDIT_DEMANDE_LISTE_RESERVATION_AUDIT);
	}

	@FXML
	private void buttonActualiserEmployeSuspenduOnAction(ActionEvent event) {
		actualiser();
	}

	@FXML
	private void buttonActualiserHistoriqueSuspensionOnAction(ActionEvent event) {
		actualiser();
	}

	@FXML
	private void buttonActualiserListEmployeOnAction(ActionEvent event) {
		actualiser();
	}

	@FXML
	private void buttonAjouterOnAction(ActionEvent event) {
		boolean validation = true;
		String message = "";
		if (textFieldLoginId.getText().isEmpty()) {
			validation = false;
			message += "Le champ login est vide\n";
		} else {
			if (!UtilitaireGestionFormulaire.caractereValide(textFieldLoginId.getText())) {
				validation = false;
				message += "Le champ login doit contenir des caracteres valides\n";
			}
		}
		if (textFieldPasswordId.getText().isEmpty()) {
			validation = false;
			message += "Le champ password est vide\n";
		} else {
			if (!UtilitaireGestionFormulaire.caractereValide(textFieldPasswordId.getText())) {
				validation = false;
				message += "Le champ password doit contenir des caracteres valides\n";
			}
		}
		if (!validation) {
			new ExceptionPersonnalisee("Erreur", message);
		} else {
			String nouveauLogin = UtilitaireGestionFormulaire.transformNomPrenom(textFieldLoginId.getText());
			String nouveauPassword = textFieldPasswordId.getText();
			client.envoyer(SIGNAL.ROOT_CREE_EMPLOYE + "-" + nouveauLogin + "-"
					+ UtilitaireGestionFormulaire.hashPassword(nouveauPassword, Statique.SALT).get());
		}
	}

	@FXML
	private void buttonSupprimerEmployeSuspenduOnAction(ActionEvent event) {
		EmployeSuspendu employe = listViewEmployeSuspendusId.getSelectionModel().getSelectedItem();
		if (employe != null) {
			client.envoyer(SIGNAL.ROOT_SUPPRIME_EMPLOYESUSPENDU + "-" + employe.getLogin());
		} else {
			new ExceptionPersonnalisee("Erreur", "Vous n'avez selectionne aucun employe");
		}
	}

	@FXML
	private void deconnexionOnAction(ActionEvent event) {
		deconnection();

	}

	private void deconnection() {
		client.envoyer(SIGNAL.VUEGESTION_DECONNEXION);
		client.closerConnection();
		this.stage.close();
		openFxmlApproprie("../loginFXML.fxml");
	}

	private void openFxmlApproprie(String string) {
		FXMLLoader loaderAjouter = new FXMLLoader(this.getClass().getResource(string));
		Stage stage = new Stage();
		AnchorPane r;
		try {
			r = (AnchorPane) loaderAjouter.load();

			Scene scene = new Scene(r);
			stage.setScene(scene);
			stage.setResizable(false);
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		if (string.equals("../loginFXML.fxml")) {
			ControleurLogin ajoutController = loaderAjouter.<ControleurLogin>getController();
			ajoutController.setStage(stage);
		}

		stage.show();

	}

	@FXML
	private void supprimerEmployeLoginOnAction(ActionEvent event) {
		Employe employe = listViewEmployeId.getSelectionModel().getSelectedItem();
		if (employe != null) {
			client.envoyer(SIGNAL.ROOT_SUPPRIME_EMPLOYE + "-" + employe.getLogin_employe());
		} else {
			new ExceptionPersonnalisee("Erreur", "Vous n'avez selectionne aucun employe");
		}
	}

	public void setStage(Stage pStage) {
		this.stage = pStage;
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				deconnection();
			}
		});

	}

	public void setData(String transformNomPrenom, Client pClient) {
		this.client = pClient;
		this.setLogin(transformNomPrenom);
		actualiser();

	}

	private void actualiser() {
		client.envoyer(SIGNAL.VUE_CONTROLEUR_AUDIT_DEMANDE_LISTE_CONNECTION_EMPLOYE);
		client.envoyer(SIGNAL.VUE_CONTROLEUR_AUDIT_DEMANDE_LISTE_RESERVATION_AUDIT);
		client.envoyer(SIGNAL.VUE_CONTROLEUR_AUDIT_DEMANDE_LISTE_EMPLOYE);
		client.envoyer(SIGNAL.VUE_CONTROLEUR_AUDIT_DEMANDE_LISTE_EMPLOYE_SUSPENDU);
		client.envoyer(SIGNAL.VUE_CONTROLEUR_AUDIT_DEMANDE_HISTORIQUE_SUSPENSION);

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		messageProperty = new SimpleStringProperty();
		messageFromServerId.textProperty().bind(messageProperty);
		tableViewConnectionAudit.setItems(observableListConnectionEmployeAudits);
		colonneLoginId.setCellValueFactory(new PropertyValueFactory<ConnectionEmployeAudit, String>("login_employe"));
		colonneColonneAuditId
				.setCellValueFactory(new PropertyValueFactory<ConnectionEmployeAudit, String>("connection_audit"));
		colonneDateDeConnectionId
				.setCellValueFactory(new PropertyValueFactory<ConnectionEmployeAudit, String>("localDateTime"));

		tableViewReservationAudit.setItems(observableListReservationAudits);
		colonneActeurId.setCellValueFactory(new PropertyValueFactory<ReservationAudit, String>("acteur"));
		colonneTypeUpdateId.setCellValueFactory(new PropertyValueFactory<ReservationAudit, String>("type_update"));
		colonneDeburAuditId.setCellValueFactory(new PropertyValueFactory<ReservationAudit, String>("debut_audit"));
		colonneFinAuditId.setCellValueFactory(new PropertyValueFactory<ReservationAudit, String>("fin_audit"));
		colonneNombreEnfantAuditId
				.setCellValueFactory(new PropertyValueFactory<ReservationAudit, Integer>("nombre_enfant_audit"));
		colonneNomPrenomClientAuditId
				.setCellValueFactory(new PropertyValueFactory<ReservationAudit, String>("nom_prenom_client_audit"));
		colonneChambreAuditId.setCellValueFactory(new PropertyValueFactory<ReservationAudit, Integer>("chambre_audit"));
		colonneDateUpdateId.setCellValueFactory(new PropertyValueFactory<ReservationAudit, String>("date_de_update"));

		listViewEmployeId.setItems(observableListEmployes);
		listViewEmployeSuspendusId.setItems(observableListEmployeSuspendus);
		listViewHistoriqueSuspensionId.setItems(observableListHistoriqueSuspensions);

		messageProperty.addListener(new ChangeListener<String>() {

			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.equals("Employe suprimé avec succes !")
						|| newValue.equals("Employe enregistré avec succes !")) {
					textFieldLoginId.setText("");
					textFieldPasswordId.setText("");
				}

			}

		});

		textFieldRechercheId.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue != null) {
					String newValueTranforme = UtilitaireGestionFormulaire.transformNomPrenom(newValue);
					List<Employe> listRecherche = null;
					listRecherche = observableListEmployes.stream()
							.filter(x -> x.getLogin_employe().startsWith(newValueTranforme))
							.collect(Collectors.toList());

					listViewEmployeId.setItems(FXCollections.observableArrayList(listRecherche));
				}

			}
		});

	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
