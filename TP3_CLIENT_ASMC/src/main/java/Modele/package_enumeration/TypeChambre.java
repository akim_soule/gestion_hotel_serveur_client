package Modele.package_enumeration;

/**
 * Enumeration pour le type de chambre.
 * @author akimsoule
 *
 */
public enum TypeChambre {
	
	SIMPLE, DOUBLE

}
