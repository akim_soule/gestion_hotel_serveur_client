package Modele.package_utilitaire;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Optional;
import java.util.logging.Level;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe permettant de gerer les formulaires client.
 * @author akimsoule
 *
 */
public class UtilitaireGestionFormulaire {

	private static final int ITERATIONS = 65536;
	private static final int KEY_LENGTH = 512;
	private static final String ALGORITHM = "PBKDF2WithHmacSHA512";
	private static final SecureRandom RAND = new SecureRandom();

	/**
	 * Methode permettant de transformer les noms et prenoms du formulaire.
	 * @param string
	 * Le nom ou le prenom entre en parametre.
	 * @return
	 * Le nom ou prenom transforme.
	 */
	public static String transformNomPrenom(String string) {

		string = string.trim().toLowerCase().replace(" ", "");

		return string;
	}

	/**
	 * Methode permettant verifier si la chaine de caracteres est valide.
	 * @param string
	 * La chaine recue en parametre.
	 * @return
	 * vrai la chaine est valide et faux sinon.
	 */
	public static boolean caractereValide(String string) {
		boolean valide = false;
		String regularExpression = "^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$";
		if (string.matches(regularExpression)) {
			valide = true;
		}
		return valide;
	}

	/**
	 * Methode permettant de hasher un mot de passe avec du sel.
	 * @param password
	 * Le mot de passe a hasher.
	 * @param salt
	 * Le sel avec lequel le mot de passe est hashe
	 * @return
	 * Le mot de passe hashe
	 */
	public static Optional<String> hashPassword(String password, String salt) {

		char[] chars = password.toCharArray();
		byte[] bytes = salt.getBytes();

		PBEKeySpec spec = new PBEKeySpec(chars, bytes, ITERATIONS, KEY_LENGTH);

		Arrays.fill(chars, Character.MIN_VALUE);

		try {
			SecretKeyFactory fac = SecretKeyFactory.getInstance(ALGORITHM);
			byte[] securePassword = fac.generateSecret(spec).getEncoded();
			// return Base64.getEncoder().encodeToString(securePassword);
			return Optional.of(Base64.getEncoder().encodeToString(securePassword));

		} catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
			System.err.println("Exception encountered in hashPassword()");
			MyLoggingMessageErreur.log(Level.WARNING, "Exception encountered in hashPassword()");
			return Optional.empty();
			// return "";

		} finally {
			spec.clearPassword();
		}
	}

	/**
	 * Methode permettant de verifier le mot de passe. 
	 * @param password
	 * Le mot de passe.
	 * @param key
	 * La cle
	 * @param salt
	 * Le sel
	 * @return
	 * retourne vrai si le mot de passe est le bon et faux dans le cas contraire.
	 */
	
	public static boolean verifyPassword(String password, String key, String salt) {
		Optional<String> optEncrypted = hashPassword(password, salt);
		if (!optEncrypted.isPresent())
			return false;
		return optEncrypted.get().equals(key);
	}

	/**
	 * Methode permettant de verifier si 3 chaines de caracteres sont valides.
	 * @param text
	 * La chaine1 a verifier
	 * @param text2
	 * La chaine2 a verifier
	 * @param text3
	 * La chaine3 a verifier
	 * @return
	 * Un message d'erreur l'une des chaines est invalide et rien dans le cas contraire.
	 */
	public static String Verification2(String text, String text2, String text3) {
		String message = "";
		if (!UtilitaireGestionFormulaire.caractereValide(text)) {
			message += "- Les caracteres du nom ne sont pas valides\n";
			// valide = false;
		}
		if (!UtilitaireGestionFormulaire.caractereValide(text2)) {
			message += "- Les caracteres du prenom ne sont pas valides\n";
			// valide = false;
		}
		if (!UtilitaireGestionFormulaire.caractereValide(text3)) {
			message += "Les caracteres du mot de passe ne sont pas valides\n";
			// valide = false;
		}
		return message;

	}

	/**
	 * Methode permettant de verifier si 3 chaines de caracteres sont vides.
	 * @param text
	 * La chaine1 a verifier
	 * @param text2
	 * La chaine2 a verifier
	 * @param text3
	 * La chaine3 a verifier
	 * @return
	 * Un message d'erreur l'une des chaines est vide et rien dans le cas contraire.
	 */
	public static String gestion(String text, String text2, String text3) {
		String message = "";
		if (text.isEmpty()) {
			message += "- Le nom n'a pas ete saisi\n";
			// ok = false;
		}
		if (text2.isEmpty()) {
			message += "- Le prenom n'a pas ete saisi\n";
			// ok = false;
		}
		if (text3.isEmpty()) {
			message += "- Le mot de passe n'a pas ete saisi\n";
			// ok = false;
		}
		return message;

	}

	/**
	 * Methode permettant de generer du sel.
	 * @param length
	 * la longueur du nombre de bits.
	 * @return
	 * le sel.
	 */
	public static Optional<String> generateSalt(int length) {

		if (length < 1) {
			// System.err.println("error in generateSalt: length must be > 0");
			MyLoggingMessageErreur.log(Level.WARNING, "error in generateSalt: length must be > 0");
			return Optional.empty();
		}
		byte[] salt = new byte[length];
		RAND.nextBytes(salt);
		return Optional.of(Base64.getEncoder().encodeToString(salt));
	}

	public static int getIterations() {
		return ITERATIONS;
	}

	public static int getKeyLength() {
		return KEY_LENGTH;
	}

	public static String getAlgorithm() {
		return ALGORITHM;
	}

	public static SecureRandom getRand() {
		return RAND;
	}

}
