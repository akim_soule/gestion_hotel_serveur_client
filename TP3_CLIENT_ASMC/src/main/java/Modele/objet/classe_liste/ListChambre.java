package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.Chambre;

/**
 * Classe permettant de representer une liste de chambres.
 * @author akimsoule
 *
 */
public class ListChambre implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La liste des chambres.
	 */
	private List<Chambre> listChambre;

	/**
	 * Constructeur de la classe.
	 * @param listChambre
	 * la liste des chambres.
	 */
	public ListChambre(List<Chambre> listChambre) {
		super();
		this.listChambre = listChambre;
	}

	public List<Chambre> getListChambre() {
		return listChambre;
	}

	public void setListChambre(List<Chambre> listChambre) {
		this.listChambre = listChambre;
	}

	@Override
	public String toString() {
		return "[listChambre=" + listChambre + "]";
	}

}
