package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.HistoriqueSuspension;

/**
 * Classe permettant de representer la liste des objets historique suspension.
 * Elle a pour principal attibut la liste des objets historique suspension.
 * @author akimsoule
 *
 */
public class ListHistoriqueSuspension implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La liste des objets historique suspension.
	 */
	private List<HistoriqueSuspension> listHistoriqueSuspensions = null;
	
	/**
	 * Principal constructeur de la classe.
	 * @param plistHistoriqueSuspensions
	 * la liste des objets historiqueSuspension.
	 */
	public ListHistoriqueSuspension(List<HistoriqueSuspension> plistHistoriqueSuspensions) {
		this.setListHistoriqueSuspensions(plistHistoriqueSuspensions);
	}

	public List<HistoriqueSuspension> getListHistoriqueSuspensions() {
		return listHistoriqueSuspensions;
	}

	public void setListHistoriqueSuspensions(List<HistoriqueSuspension> listHistoriqueSuspensions) {
		this.listHistoriqueSuspensions = listHistoriqueSuspensions;
	}

	@Override
	public String toString() {
		return "ListHistoriqueSuspension [listHistoriqueSuspensions=" + listHistoriqueSuspensions + "]";
	}
	
	
	
	

}
