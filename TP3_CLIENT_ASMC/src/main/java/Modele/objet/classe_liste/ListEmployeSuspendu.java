package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.EmployeSuspendu;


/**
 * Classe permettant de representer la liste des employes suspendus.
 * @author akimsoule
 *
 */
public class ListEmployeSuspendu implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La liste des employes suspendus.
	 */
	private List <EmployeSuspendu> listEmployeSuspendu;

	/**
	 * Principal constructeur de la classe.
	 * @param listEmployeSuspendu
	 * la liste des employes suspendus.
	 */
	public ListEmployeSuspendu(List<EmployeSuspendu> listEmployeSuspendu) {
		this.listEmployeSuspendu = listEmployeSuspendu;
	}

	public List<EmployeSuspendu> getListEmployeSuspendu() {
		return listEmployeSuspendu;
	}

	public void setListEmployeSuspendu(List<EmployeSuspendu> listEmployeSuspendu) {
		this.listEmployeSuspendu = listEmployeSuspendu;
	}
	
	
	
	

}
