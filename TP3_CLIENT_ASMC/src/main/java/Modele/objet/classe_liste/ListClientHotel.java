package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.ClientHotel;

/**
 * Classe representant une liste des clients de l'hotel.
 * @author akimsoule
 *
 */
public class ListClientHotel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La liste des clients de l'hotel.
	 */
	private List<ClientHotel> list = null;

	/**
	 * Principal constructeur de la classe.
	 * @param list
	 * La liste des clients de l'hotel.
	 */
	public ListClientHotel(List<ClientHotel> list) {
		super();
		this.list = list;
	}

	public List<ClientHotel> getList() {
		return list;
	}

	public void setList(List<ClientHotel> list) {
		this.list = list;
	}

}
