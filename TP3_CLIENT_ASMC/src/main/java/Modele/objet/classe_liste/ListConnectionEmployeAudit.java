package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.ConnectionEmployeAudit;

/**
 * Classe permettant de representer la liste des objets connectionEmployeAudit.
 * Elle a pour principal attribut la liste des connectionEmployeAudit.
 * @author akimsoule
 *
 */
public class ListConnectionEmployeAudit implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * La liste des objets ConnectionEmployeAudit.
	 */
	private List<ConnectionEmployeAudit> listConnectionEmployeAudit;

	/**
	 * Constructeur de la classe.
	 * @param listConnectionEmployeAudit
	 * la liste des objets ConnectionEmployeAudit.
	 */
	public ListConnectionEmployeAudit(List<ConnectionEmployeAudit> listConnectionEmployeAudit) {
		this.listConnectionEmployeAudit = listConnectionEmployeAudit;
	}

	public List<ConnectionEmployeAudit> getListConnectionEmployeAudit() {
		return listConnectionEmployeAudit;
	}

	public void setListConnectionEmployeAudit(List<ConnectionEmployeAudit> listConnectionEmployeAudit) {
		this.listConnectionEmployeAudit = listConnectionEmployeAudit;
	}

}
