package Modele.objet.classe_liste;

import java.io.Serializable;
import java.util.List;

import Modele.objet.classe_java.ReservationAudit;

/**
 * Classe representant la liste des objets reservationAudit.
 * Elle a pour principal attribut la liste des objets reservationAudit.
 * @author akimsoule
 *
 */
public class ListReservationAudit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La liste des objets reservationAudit.
	 */
	private List<ReservationAudit> listReservationAudit;

	/**
	 * Principal constructeur de la classe.
	 * @param listReservationAudit
	 * la liste des objets reservationAudit.
	 */
	public ListReservationAudit(List<ReservationAudit> listReservationAudit) {
		this.listReservationAudit = listReservationAudit;
	}

	public List<ReservationAudit> getListReservationAudit() {
		return listReservationAudit;
	}

	public void setListReservationAudit(List<ReservationAudit> listReservationAudit) {
		this.listReservationAudit = listReservationAudit;
	}
	
}
