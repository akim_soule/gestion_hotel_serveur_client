package Modele.objet.classe_java;

import java.io.Serializable;

/**
 * Classe permettant de representer l'historique des suspensions.
 * @author akimsoule
 *
 */
public class HistoriqueSuspension implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * L'id de l'historique.
	 */
	private int id_employe_suspendu;
	/**
	 * Le login de l'employe suspendu.
	 */
	private String login_employe_suspendu_audit;
	/**
	 * L'audit qui peut prendre les valeurs (SUSPENDU ou SUSPENSION_LEVEE).
	 */
	private String audit;
	/**
	 * La date de declenchement.
	 */
	private String date_de_declenchement;

	/**
	 * Principal constructeur de la classe.
	 * @param id_employe_suspendu
	 * l'id de l'employe.
	 * @param login_employe_suspendu_audit
	 * le login de l'employe suspendu.
	 * @param audit
	 * l'audit (SUSPENDU ou SUSPENSION_LEVEE).
	 * @param date_de_declenchementl
	 * la date de declenchement.
	 */
	public HistoriqueSuspension(int id_employe_suspendu, String login_employe_suspendu_audit, String audit,
			String date_de_declenchementl) {
		super();
		this.id_employe_suspendu = id_employe_suspendu;
		this.login_employe_suspendu_audit = login_employe_suspendu_audit;
		this.audit = audit;
		this.date_de_declenchement = date_de_declenchementl;
	}

	public int getId_employe_suspendu() {
		return id_employe_suspendu;
	}

	public void setId_employe_suspendu(int id_employe_suspendu) {
		this.id_employe_suspendu = id_employe_suspendu;
	}

	public String getLogin_employe_suspendu_audit() {
		return login_employe_suspendu_audit;
	}

	public void setLogin_employe_suspendu_audit(String login_employe_suspendu_audit) {
		this.login_employe_suspendu_audit = login_employe_suspendu_audit;
	}

	public String getAudit() {
		return audit;
	}

	public void setAudit(String audit) {
		this.audit = audit;
	}

	public String getDate_de_declenchement() {
		return date_de_declenchement;
	}

	public void setDate_de_declenchement(String date_de_declenchementl) {
		this.date_de_declenchement = date_de_declenchementl;
	}

	@Override
	public String toString() {
		return id_employe_suspendu + ", "
				+ login_employe_suspendu_audit + ", " + audit + ", "
				+ date_de_declenchement;
	}

}
