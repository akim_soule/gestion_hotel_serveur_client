package Modele.objet.classe_java;

import java.time.LocalDateTime;

import Modele.donnees.Statique;
import Modele.package_enumeration.TypeChambre;
import Modele.package_utilitaire.TraitementDate;

/**
 * Classe permettant de representer une chambre.
 * Elle a pour principals attributs le numero, le type de chambre, les dates de debut et de fin.
 * @author akimsoule
 *
 */
public class Chambre {
	
	/**
	 * Le numero.
	 */
	private int numero;
	/**
	 * Le type de chambre.
	 */
	private TypeChambre type;
	/**
	 * La date de debut.
	 */
	private LocalDateTime localDateTimeDebut;
	/**
	 * La date de fin.
	 */
	private LocalDateTime localDateTimeFin;

	/**
	 * Principal constructeur de la classe.
	 * @param i
	 * i etant le numero de chambre.
	 */
	public Chambre(int i) {
		
		if (i >= Statique.PLUS_PETITE_CHAMBRE && i <= Statique.PLUS_GRANDE_CHAMBRE) {
			this.numero = i;
			if ((i / 100) % 2 == 0) {
				this.setType(TypeChambre.DOUBLE);
			} else {
				this.setType(TypeChambre.SIMPLE);
			}
		}
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public LocalDateTime getLocalDateTimeDebut() {
		return localDateTimeDebut;
	}

	public void setLocalDateTimeDebut(LocalDateTime localDateTimeDebut) {
		this.localDateTimeDebut = localDateTimeDebut;
	}

	public LocalDateTime getLocalDateTimeFin() {
		return localDateTimeFin;
	}

	public void setLocalDateTimeFin(LocalDateTime localDateTimeFin) {
		this.localDateTimeFin = localDateTimeFin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localDateTimeDebut == null) ? 0 : localDateTimeDebut.hashCode());
		result = prime * result + ((localDateTimeFin == null) ? 0 : localDateTimeFin.hashCode());
		result = prime * result + numero;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chambre other = (Chambre) obj;
		if (numero != other.numero)
			return false;
		return true;
	}

	public boolean equalsComplet(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chambre other = (Chambre) obj;
		if (localDateTimeDebut == null) {
			if (other.localDateTimeDebut != null)
				return false;
		} else if (!localDateTimeDebut.equals(other.localDateTimeDebut))
			return false;
		if (localDateTimeFin == null) {
			if (other.localDateTimeFin != null)
				return false;
		} else if (!localDateTimeFin.equals(other.localDateTimeFin))
			return false;
		if (numero != other.numero)
			return false;
		return true;
	}

	public TypeChambre getType() {
		return type;
	}

	public void setType(TypeChambre type) {
		this.type = type;
	}

	/**
	 * Methode permettant de definir la date de debut et la date de fin.
	 * @param dateDebut
	 * la date de debut.
	 * @param dateFin
	 * la date de fin.
	 */
	public void setDateTimeDebutEtFin(String dateDebut, String dateFin) {
		this.setLocalDateTimeDebut(
				LocalDateTime.of(TraitementDate.getAnnee(dateDebut), TraitementDate.getMois(dateDebut),
						TraitementDate.getJour(dateDebut), TraitementDate.getHeure(dateDebut),
						TraitementDate.getMinute(dateDebut), TraitementDate.getSeconde(dateDebut)));

		this.setLocalDateTimeFin(LocalDateTime.of(TraitementDate.getAnnee(dateFin), TraitementDate.getMois(dateFin),
				TraitementDate.getJour(dateFin), TraitementDate.getHeure(dateFin), TraitementDate.getMinute(dateFin),
				TraitementDate.getSeconde(dateFin)));

	}

	@Override
	public String toString() {
		return "Chambre [numero=" + numero + ", localDateTimeDebut=" + localDateTimeDebut + ", localDateTimeFin="
				+ localDateTimeFin + "]";
	}
}
