package Modele;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import Controleur.ControleurAudit;
import Controleur.ControleurLogin;
import Controleur.ControleurRechercheClientFXML;
import Controleur.ControleurVueGestion;
import Modele.objet.classe_java.ClientHotel;
import Modele.objet.classe_java.ConnectionEmployeAudit;
import Modele.objet.classe_java.Employe;
import Modele.objet.classe_java.EmployeSuspendu;
import Modele.objet.classe_java.HistoriqueSuspension;
import Modele.objet.classe_java.Reservation;
import Modele.objet.classe_java.ReservationAudit;
import Modele.objet.classe_liste.ListClientHotel;
import Modele.objet.classe_liste.ListConnectionEmployeAudit;
import Modele.objet.classe_liste.ListEmploye;
import Modele.objet.classe_liste.ListEmployeSuspendu;
import Modele.objet.classe_liste.ListHistoriqueSuspension;
import Modele.objet.classe_liste.ListReservation;
import Modele.objet.classe_liste.ListReservationAudit;
import Modele.package_logger.MyLoggingConnectionIPetPort;
import Modele.package_logger.MyLoggingMessageErreur;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Classe representant un client reseau.
 * Elle a pour principals attributs : un outputstream, un inputstream et un boolean indiquant s'il est connecté ou pas.
 * @author akimsoule
 *
 */
public class Client {

	/**
	 * le socket client.
	 */
	private SSLSocket socketClient = null;
	/**
	 * La frabrique de socket ssl
	 */
	private SSLSocketFactory f = (SSLSocketFactory) SSLSocketFactory.getDefault();
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	private BooleanProperty connected;

	/**
	 * Constructeur principal de la classe ayant pour attributs l'adresse ip du serveur et son port d'ecoute.
	 * @param host
	 * L'adresse ip du serveur.
	 * @param port
	 * Le port d'écoute du serveur.
	 */
	public Client(String host, int port) {
		try {
			socketClient = (SSLSocket) f.createSocket(host, port);
			socketClient.startHandshake();


			inputStream = new ObjectInputStream(socketClient.getInputStream());
			outputStream = new ObjectOutputStream(socketClient.getOutputStream());
			
			connected = new SimpleBooleanProperty(true);
			modeReceptionMessage();
			MyLoggingConnectionIPetPort.log(Level.INFO, "Conection au serveur "+socketClient.getInetAddress().toString()+"-----"+socketClient.getPort());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
	}

	/**
	 * Methode permettant d'envoyer des objets.
	 * @param object
	 * L'objet a envoyer.
	 */
	public void envoyer(Object object) {
		try {
			outputStream.writeObject(object);
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
	}

	/**
	 * Methode permettant de gerer les objets recus sur le serveur.
	 */
	public void modeReceptionMessage() {
		new Thread(() -> {
			boolean connected = true;
			while (connected) {
				try {
					Object object = inputStream.readObject();
					while (object != null) {

						if (object.getClass() == String.class) {
							String response = (String) object;
							System.out.println(response);

							if (response.toString().startsWith("CVAUDIT")) {
								ControleurAudit.messageProperty.setValue(response.split("-")[1]);
							}
							if (response.toString().startsWith("CVUEGES")) {
								Platform.runLater(() -> {
									ControleurVueGestion.messageProperty2.setValue(response.split("-")[1]);
								});
							}
							if (response.toString().startsWith("CV")) {
								Platform.runLater(() -> {
									ControleurLogin.messageProperty.setValue(response.split("-")[1]);
								});
								// Client.setConnected(new SimpleBooleanProperty(true));
							}
						}

						if (object.getClass() == ListClientHotel.class) {
							ListClientHotel listClient = (ListClientHotel) object;
							List<ClientHotel> list = listClient.getList();

							Platform.runLater(() -> {
								ControleurVueGestion.observableListClient.clear();
								for (int i = 0; i < list.size(); i++) {
									ControleurVueGestion.observableListClient.add(list.get(i).toString());
								}
								ControleurRechercheClientFXML.observableListClient.clear();
								for (int i = 0; i < list.size(); i++) {
									ControleurRechercheClientFXML.observableListClient.add(list.get(i).toStringId());
								}
							});

						}
						if (object.getClass() == ListReservation.class) {
							ListReservation listReservation = (ListReservation) object;
							List<Reservation> list = listReservation.getListReservations();
							Platform.runLater(() -> {
								ControleurVueGestion.observableListReservation.setAll(list);
							});

						}
						if (object.getClass() == ListConnectionEmployeAudit.class) {
							ListConnectionEmployeAudit listConnectionEmployeAudit = (ListConnectionEmployeAudit) object;
							List<ConnectionEmployeAudit> list = listConnectionEmployeAudit
									.getListConnectionEmployeAudit();
							Platform.runLater(() -> {
								ControleurAudit.observableListConnectionEmployeAudits.setAll(list);
							});

						}
						if (object.getClass() == ListReservationAudit.class) {
							ListReservationAudit listReservationAudit = (ListReservationAudit) object;
							List<ReservationAudit> list = listReservationAudit.getListReservationAudit();
							Platform.runLater(() -> {
								ControleurAudit.observableListReservationAudits.setAll(list);
							});

						}
						if (object.getClass() == ListEmploye.class) {
							ListEmploye list = (ListEmploye) object;
							List<Employe> liEmploye = list.getListEmploye();
							liEmploye.remove(0);
							Platform.runLater(() -> {
								ControleurAudit.observableListEmployes.setAll(liEmploye);
							});
						}

						if (object.getClass() == ListEmployeSuspendu.class) {
							ListEmployeSuspendu list = (ListEmployeSuspendu) object;
							List<EmployeSuspendu> liEmployeSuspendus = list.getListEmployeSuspendu();
							Platform.runLater(() -> {
								ControleurAudit.observableListEmployeSuspendus.setAll(liEmployeSuspendus);
							});

						}
						if (object.getClass() == ListHistoriqueSuspension.class) {
							ListHistoriqueSuspension list = (ListHistoriqueSuspension) object;
							List<HistoriqueSuspension> liEmployeSuspendus = list.getListHistoriqueSuspensions();
							Platform.runLater(() -> {
								ControleurAudit.observableListHistoriqueSuspensions.setAll(liEmployeSuspendus);
							});

						}

						object = inputStream.readObject();
					}
				} catch (IOException e) {
					connected = false;
				} catch (ClassNotFoundException e) {
					connected = false;
				}
			}
		}).start();
	}

	/**
	 * Methode permettant de fermer la connexion.
	 */
	public void closerConnection() {
		try {
			MyLoggingConnectionIPetPort.log(Level.INFO, "Deconnexion du "+socketClient.getInetAddress().toString()+"-----"+socketClient.getPort());
			outputStream = null;
			inputStream = null;
			socketClient.close();
			this.connected.setValue(false);
		} catch (IOException e) {
			e.printStackTrace();
			MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
	}

	public BooleanProperty getConnected() {
		return connected;
	}

	public void setConnected(BooleanProperty connected) {
		this.connected = connected;
	}

}
